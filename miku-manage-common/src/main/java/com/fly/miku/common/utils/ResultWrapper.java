package com.fly.miku.common.utils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fly.miku.common.resenum.RespEnum;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class ResultWrapper<T>   implements Serializable {

private static final long serialVersionUID = 1L;
	
	private int code=0;
	private String message="";
	private T entity;
	
	public ResultWrapper(RespEnum bw){
		this.code= bw.getCode();
		this.message= bw.getDesc();
	}
	public ResultWrapper(RespEnum bw, String message, T entity){
		this.code= bw.getCode();
		this.message= message;
		this.entity= entity;
	}

	public ResultWrapper(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public ResultWrapper(RespEnum bw, String message){
		this.code= bw.getCode();
		this.message= message;
	}
	public ResultWrapper(T entity,RespEnum bw){
		this.code= bw.getCode();
		this.entity= entity;
		this.message = bw.getDesc();
	}
	
	public ResultWrapper(RespEnum bw,T entity){
		this.code= bw.getCode();
		this.entity= entity;
		this.message = bw.getDesc();
	}
	

}
