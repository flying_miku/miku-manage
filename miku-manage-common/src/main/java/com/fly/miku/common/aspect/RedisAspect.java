package com.fly.miku.common.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fly.miku.common.exception.FLYException;

/**
 * Redis切面处理类
 *
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-07-17 23:30
 */
@Aspect
@Component
public class RedisAspect {
    private Logger logger = LoggerFactory.getLogger(getClass());
    //是否开启redis缓存  true开启   false关闭
    @Value("${miku.redis.open: false}")
    private boolean open;

    @Around("execution(* com.fly.miku.common.utils.RedisUtils.*(..))")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Object result = null;
        if(open){
            try{
                result = point.proceed();
            }catch (Exception e){
                logger.error("redis error", e);
                throw new FLYException("Redis服务异常");
            }
        }
        return result;
    }
}
