package com.fly.miku.common.exception;

import com.fly.miku.common.resenum.RespEnum;

import lombok.Getter;

/**
 * 自定义异常
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年10月27日 下午10:11:27
 */
public class FLYException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	@Getter
	private String message;
	@Getter
	private int code;
    
    public FLYException(RespEnum respEnum) {
		super(respEnum.getDesc());
		this.code = respEnum.getCode();
		this.message = respEnum.getDesc();
	}
	
	public FLYException(RespEnum respEnum, Throwable e) {
		super(respEnum.getDesc(), e);
		this.code = respEnum.getCode();
		this.message = respEnum.getDesc();
	}
	
	public FLYException(RespEnum respEnum, Object  arguments){
		super(respEnum.getDesc());
		this.code = respEnum.getCode();
		this.message = respEnum.getDesc();
	}
	public FLYException(RespEnum respEnum, Object  arguments,Throwable e){
		super(respEnum.getDesc(), e);
		this.code = respEnum.getCode();
		this.message = respEnum.getDesc();
	}
	
	public FLYException(String message){
		super(message);
		this.code = 500;
		this.message = message;
	}
	
	public FLYException(int code,String message){
		super(message);
		this.code = code;
		this.message = message;
	}
	
	public FLYException(String message,Throwable e){
		super(message,e);
		this.code = 500;
		this.message = message;
	}
	
	public FLYException(int code,String message,Throwable e){
		super(message,e);
		this.code = code;
		this.message = message;
	}
	

	
	
}
