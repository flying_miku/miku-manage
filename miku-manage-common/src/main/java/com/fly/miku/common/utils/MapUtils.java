package com.fly.miku.common.utils;

import java.util.HashMap;


/**
 * Map工具类
 *
 * @author Flying flying_miku@sina.com
 * @since 1.0.0
 */
public class MapUtils extends HashMap<String, Object> {

    @Override
    public MapUtils put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
