package com.fly.miku.common.resenum;

import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;

import lombok.Getter;

public enum RespEnum {
	
	/**
	 * System
	 */
	SUCCESS(0, "请求成功","success"),
	RECORD_EXISTED_DATABASE(201,"数据库中已存在该记录","record existed database"),
	CONTAIN_ILLEGAL_CHARACTERS(401,"包含非法字符","contain illegal characters"),
	DATA_PERMISSIONS_INTERFACE_CAN_ONLY_BE_MAP_TYPE_PARAMETERS_AND_CANT_BE_NULL(402,"数据权限接口，只能是Map类型参数，且不能为NULL","data_permissions interface can only be map type parameters and cant be null"),
	GAIN_PARAMETER_FAILURE(403,"获取参数失败","gain parameter failure"),
	NOT_FOUND_ERROR(404, "页面不存在","page does not exist"),
	REQUEST_PARAM_ERROR(405, "请求参数不正确","request param error"),
	ILLEGAL_REQUEST(406,"非法请求","illegal request"),
	EXISTENCE(407,"已存在","existence"),
	VERIFICATION_CODE_HAS_FAILED(408,"验证码已失效","verification code has failed"),
	SERVER_ERROR(500, "服务器出正在维护，请稍后重试","server error"),
	GET_CONFIGURATION_FILE_FAILURE(501,"获取配置文件失败","get configuration file failure"),
	
	CREATE_TIME_TASK_FAILURE(601,"创建时间任务失败","create time task failure"),
	DELETE_TIMED_TASK_FAILURE(602,"删除定时任务失败","delete timed task failure"),
	GETTING_THE_EXCEPTION_OF_THE_TIMING_TASK_CRONTRIGGER(603,"获取定时任务CronTrigger出现异常","getting the exception of the timing task crontrigger"),
	PAUSE_TIME_TASK_FAILURE(604,"暂停定时任务失败","pause time task failure"),
	RECOVERY_TIME_TASK_FAILURE(605,"恢复定时任务失败","recovery time task failure"),
	FAIL_TO_PERFORM_A_TIMED_TASK(606,"执行定时任务失败","fail to perform a timed task"),
	FAILURE_TO_EXECUTE_THE_TIMING_TASK_IMMEDIATELY(607,"立即执行定时任务失败","failure to execute the timing task immediately"),
	UPDATE_THE_TIMED_TASK_FAILURE(608,"更新定时任务失败","update the timed task failure"),
	
	
	UPLOAD_FILE_FAILED__PLEASE_CHECK_SEVEN_BOVINE_CONFIGURATION_INFORMATION(701,"上传文件失败，请核对七牛配置信息","upload file failed  please check seven bovine configuration information"),
	UPLOAD_FILE_FAILURE(702,"上传文件失败","upload file failure"),
	UPLOADING_FILES_CAN_NOT_BE_EMPTY(703,"上传文件不能为空","uploading files can not be empty"),
	UPLOAD_FILE_FAILED_PLEASE_CHECK_CONFIGURATION_INFORMATION(704,"上传文件失败，请检查配置信息","upload file failed please check configuration information"),
	
	REDIS_SERVICE_EXCEPTION(801,"Redis服务异常","redis service exception"),

	
	
	/***
	 * Meeu
	 */
	MENU_NAME_NOT_NULL(1001,"菜单名称不能为空","menu name not null"),
	SUPERIOR_MENUS_CANNOT_BE_EMPTY(1002,"上级菜单不能为空","superior menus cannot be empty"),
	SUPERIOR_MENU_URL_NOT_NULL(1003,"菜单URL不能为空","superior menu url not null"),
	SUPERIOR_MENU_IS_ONLY_A_DIRECTORY_TYPE(1004,"上级菜单只能为目录类型","superior menu is only a directory type"),
	THE_SUPERIOR_MENU_IS_ONLY_FOR_THE_MENU_TYPE(1005,"上级菜单只能为菜单类型","the superior menu is only for the menu type"),
	SYSTEM_MENU_CAN_NOT_BE_DELETED(1006,"系统菜单，不能删除","system menu can not be deleted"),
	PLEASE_DELETE_THE_SUBMENU_OR_BUTTON_FIRST(1007,"请先删除子菜单或按钮","please delete the submenu or button first"),
	NO_AUTHORITY_PLEASE_CONTACT_THE_ADMINISTRATOR_TO_AUTHORIZE(1008,"没有权限，请联系管理员授权","no authority please contact the administrator to authorize"),
	DATA_ILLEGALITY(1009,"数据不合法","data_illegality"),
	PLEASE_DELETE_THE_SUB_DEPARTMENT_FIRST(1010,"请先删除子部门","please delete the sub department first"),

	
	/**
	 * System User
	 */
	INCORRECT_ACCOUNT_OR_PASSWORD(2001,"账号或密码不正确","account or password error"),
	THE_ACCOUNT_HAS_BEEN_LOCKED_PLEASE_CONTACT_THE_ADMINISTRATOR(2002,"账号已被锁定,请联系管理员","the account has been locked please contact the administrator"),
	ACCOUNT_VERIFICATION_FAILURE(2003,"账户验证失败","account verification failure"),
	VALIDATION_CODE_IS_INCORRECT(2004,"验证码不正确","validation code is incorrect"),
	INCORRECT_ORIGINAL_PASSWORD(2005,"原密码不正确","incorrect original password"),
	THE_SYSTEM_ADMINISTRATOR_CANT_DELETE(2006,"系统管理员不能删除","the system administrator cant delete"),
	CURRENT_USER_CANNOT_DELETE(2007,"当前用户不能删除","current user cannot delete"),
	
	/**
	 * File 
	 */
	PLEASE_SELECT_THE_DIRECTORY(3000,"请选择目录","please select the directory")	;
	;

	@Getter
	private final int code;
	
	
	private final String desc;
	
	public final String getDesc(){
		return LocaleContextHolder.getLocale().equals(Locale.US)?this.enDesc:this.desc;
	}
	
	private final String enDesc;
	
	private RespEnum(int code, String zhDesc,String enDesc) {
		this.code = code;
		this.desc = zhDesc;
		this.enDesc = enDesc;
	}
	
	
}
