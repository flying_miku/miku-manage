package com.fly.miku.common.validator;

import org.apache.commons.lang.StringUtils;

import com.fly.miku.common.exception.FLYException;

/**
 * 数据校验
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-03-23 15:50
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new FLYException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new FLYException(message);
        }
    }
}
