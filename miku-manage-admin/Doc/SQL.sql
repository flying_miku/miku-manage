#
#
#								MYSQL数据库
#
#
# Structure for DATABASE "MIKUStudio_CARD" 
#
DROP DATABASE if EXISTS `MIKUStudio_CARD`;
CREATE DATABASE IF NOT EXISTS `MIKUStudio_CARD` DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
USE `MIKUStudio_CARD`;



#
# structure for table "t_user_admin"
#
drop table if exists `t_user_admin`;
create table `t_user_admin` # 系统后台人员表
(
	`f_user_admin_id`    			int(11)     			not null 		auto_increment 	comment '自增长,作为表主键',
	`f_user_admin_uid`   			varchar(50) 			not null 						comment '用户id',
	`f_user_password`    			varchar(35) 			not null 						comment '用户密码',
	`f_salt`    					varchar(20) 			not null 						comment '私盐',
	`f_mobile`    					varchar(20)				default null					comment '手机号',
	`f_email`    					varchar(50)				default null					comment 'Email',
	`f_user_last_login`  			datetime    			default null 					comment '最后一次登录时间',
	`f_user_last_ip`     			varchar(15) 			default null 					comment '用户最后一次登录ip',
	`f_user_role_id`				int(11)					not null						comment '用户角色id',
	`f_department_info_id`        	int(11)					not null						comment '部门id',
	`f_register_date`				datetime				default null					comment '用户注册时间',
	`f_photo_url`					varchar(200)			default null					comment '用户头像',
	`f_state`						tinyint(3)				default '0'						comment '用户状态    0:未激活   1:正常用户  2:锁定用户  3:已删除',
	`f_comment`						varchar(200) 			default null					comment '用户备注',
	primary key (`f_user_admin_id`)
) engine=innodb default charset=utf8 comment='系统后台人员表';
insert into t_user_admin (f_user_admin_id,f_user_admin_uid,f_user_password,f_salt,f_mobile,f_email,f_user_last_login,f_user_last_ip,f_user_role_id,f_department_info_id,f_register_date,f_photo_url,f_state,f_comment)value(1,'admin','202cb962ac59075b964b07152d234b70','salt','18699829705','363131422@qq.com','1999-09-09 22:22:22','192.168.192.168',1,1,'1990-01-01',null,1,null);


#
# structure for table "t_department_info"
#
drop table if exists `t_department_info`;
create table `t_department_info` # 部门表
(
	`f_department_info_id`    	int(11)     			not null 		auto_increment 	comment '自增长,作为表主键',
	`f_department_parentid`    	int(11)     			not null 					 	comment '上一级部门ID',
	`f_department_info_name`   	varchar(20) 			not null 						comment '部门名称',
	`f_isdelete`				tinyint(3)				default '0'						comment '是否删除',
	primary key (`f_department_info_id`)
) engine=innodb default charset=utf8 comment='部门表';
insert into t_department_info (f_department_info_id,f_department_parentid,f_department_info_name,f_isdelete) value(1,0,'系统管理员组',0);



#
# structure for table "t_role_info"
#
drop table if exists `t_role_info`;
create table `t_role_info` # 角色表
(
	`f_role_id`    				int(11)     			not null 		auto_increment 	comment '自增长,作为表主键',
	`f_role_name`   			varchar(20) 			not null 						comment '角色名称',
	`f_department_id`			int(11)					not null						comment '部门id',
	`f_isdelete`				tinyint(3)				default '0'						comment '是否删除',
	primary key (`f_role_id`)
) engine=innodb default charset=utf8 comment='角色表';
insert into t_role_info (f_role_id,f_role_name,f_department_id,f_isdelete) value(1,'系统管理员',1,0);
insert into t_role_info (f_role_id,f_role_name,f_department_id,f_isdelete) value(2,'管理员',1,0);



#
# structure for table "t_permissions_info"
#
drop table if exists `t_permissions_info`;
create table `t_permissions_info` # 权限表
(
	`f_permissions_id`    			int(11)     			not null 		auto_increment 	comment '自增长,作为表主键',
	`f_permissions_type`			tinyint(3)				default '0'						comment '权限类型 0:菜单,1:按钮',
	`f_permissions_pid`				int(11)					default '0'						comment '父类型id',
	primary key (`f_permissions_id`)
) engine=innodb default charset=utf8 comment='权限表';
insert into t_permissions_info (f_permissions_id,f_permissions_type,f_permissions_pid) value(1,0,1);
insert into t_permissions_info (f_permissions_id,f_permissions_type,f_permissions_pid) value(2,0,2);
insert into t_permissions_info (f_permissions_id,f_permissions_type,f_permissions_pid) value(3,0,3);
insert into t_permissions_info (f_permissions_id,f_permissions_type,f_permissions_pid) value(4,0,4);
insert into t_permissions_info (f_permissions_id,f_permissions_type,f_permissions_pid) value(5,0,5);
insert into t_permissions_info (f_permissions_id,f_permissions_type,f_permissions_pid) value(6,0,6);
insert into t_permissions_info (f_permissions_id,f_permissions_type,f_permissions_pid) value(7,0,7);
insert into t_permissions_info (f_permissions_id,f_permissions_type,f_permissions_pid) value(8,0,8);
insert into t_permissions_info (f_permissions_id,f_permissions_type,f_permissions_pid) value(9,0,9);
insert into t_permissions_info (f_permissions_id,f_permissions_type,f_permissions_pid) value(10,0,10);
insert into t_permissions_info (f_permissions_id,f_permissions_type,f_permissions_pid) value(11,0,11);
insert into t_permissions_info (f_permissions_id,f_permissions_type,f_permissions_pid) value(12,0,12);


#
# structure for table "t_role_permissions"
#
drop table if exists `t_role_permissions`;
create table `t_role_permissions` # 角色权限关系表
(
	`f_role_id`    					int(11)     			not null 					 	comment '角色id',
	`f_permissions_id`				int(11)					not null						comment '权限id',
	`f_fix_filter` 					varchar(500) 			default null 					comment '固定过滤条件'
) engine=innodb default charset=utf8 comment='角色权限关系表';
insert into t_role_permissions (f_role_id,f_permissions_id,f_fix_filter)value(1,1,'');
insert into t_role_permissions (f_role_id,f_permissions_id,f_fix_filter)value(1,2,'');
insert into t_role_permissions (f_role_id,f_permissions_id,f_fix_filter)value(1,3,'');
insert into t_role_permissions (f_role_id,f_permissions_id,f_fix_filter)value(1,4,'');
insert into t_role_permissions (f_role_id,f_permissions_id,f_fix_filter)value(1,5,'');
insert into t_role_permissions (f_role_id,f_permissions_id,f_fix_filter)value(1,6,'');
insert into t_role_permissions (f_role_id,f_permissions_id,f_fix_filter)value(1,7,'');
insert into t_role_permissions (f_role_id,f_permissions_id,f_fix_filter)value(1,8,'');
insert into t_role_permissions (f_role_id,f_permissions_id,f_fix_filter)value(1,9,'');
insert into t_role_permissions (f_role_id,f_permissions_id,f_fix_filter)value(1,10,'');
insert into t_role_permissions (f_role_id,f_permissions_id,f_fix_filter)value(1,11,'');
insert into t_role_permissions (f_role_id,f_permissions_id,f_fix_filter)value(1,12,'');

#
# structure for table "t_admin_user_login"
#
drop table if exists `t_admin_user_login`;
create table `t_admin_user_login` # 后台用户登录历史表
(
	`f_admin_user_login_id`   		int(11)     			not null 		auto_increment 	comment '自增长,作为表主键',
	`f_user_admin_id`	  			int(11)     			not null 						comment 'admin用户账号表主键',
	`f_login_time`      			datetime    			not null 						comment 'admin用户登录时间',
	`f_login_ip`        			varchar(50) 			not null 						comment 'admin用户登录ip',
	`f_login_result`    			tinyint(3)  			not null 						comment '登录结果',
	primary key (`f_admin_user_login_id`)
) engine=innodb default charset=utf8 comment='后台用户登录历史表'; 



#
# structure for table "t_menu"
#
drop table if exists `t_menu`;
create table `t_menu` # 菜单管理
(
	`f_menu_id` 				int(11) 				not null 		auto_increment 	comment '菜单id',
	`f_menu_name` 				varchar(50) 			not null 						comment '菜单名称',
	`f_url` 					varchar(50) 			default null 					comment '链接地址',
	`f_parentid` 				int(11)  				default null 					comment '父类id',
	`f_display` 				int(1) 					default null 					comment '是否可见',
	`f_index` 					int(9) 					not null 						comment '序号',
	`f_isdelete`				tinyint(3)				default '0'						comment '是否删除',	
`f_menu_level`				int(9)          		not NULL								comment '菜单等级',
	`f_ico`						varchar(50)           	default 'MENU'								comment '菜单图标',
	primary key (`f_menu_id`)
) engine=innodb default charset=utf8 comment='菜单表管理'; 
insert into t_menu (f_menu_id,f_menu_name,f_url,f_parentid,f_display,f_index,f_isdelete,f_menu_level)value(1,'系统管理','',0,1,1,0,0);
insert into t_menu (f_menu_id,f_menu_name,f_url,f_parentid,f_display,f_index,f_isdelete,f_menu_level)value(2,'菜单管理','',1,1,1,0,1);
insert into t_menu (f_menu_id,f_menu_name,f_url,f_parentid,f_display,f_index,f_isdelete,f_menu_level)value(3,'表单管理','',1,1,1,0,1);
insert into t_menu (f_menu_id,f_menu_name,f_url,f_parentid,f_display,f_index,f_isdelete,f_menu_level)value(4,'报表设置','',1,1,1,0,1);
insert into t_menu (f_menu_id,f_menu_name,f_url,f_parentid,f_display,f_index,f_isdelete,f_menu_level)value(5,'用户管理','',1,1,1,0,1);
insert into t_menu (f_menu_id,f_menu_name,f_url,f_parentid,f_display,f_index,f_isdelete,f_menu_level)value(6,'角色管理','',1,1,1,0,1);
insert into t_menu (f_menu_id,f_menu_name,f_url,f_parentid,f_display,f_index,f_isdelete,f_menu_level)value(7,'部门管理','',1,1,1,0,1);
insert into t_menu (f_menu_id,f_menu_name,f_url,f_parentid,f_display,f_index,f_isdelete,f_menu_level)value(8,'文件管理','',1,1,1,0,1);
insert into t_menu (f_menu_id,f_menu_name,f_url,f_parentid,f_display,f_index,f_isdelete,f_menu_level)value(9,'数据源管理','',1,1,1,1,1);
insert into t_menu (f_menu_id,f_menu_name,f_url,f_parentid,f_display,f_index,f_isdelete,f_menu_level)value(10,'日志管理','',1,1,1,1,1);
insert into t_menu (f_menu_id,f_menu_name,f_url,f_parentid,f_display,f_index,f_isdelete,f_menu_level)value(11,'模板管理','',1,1,1,1,1);
insert into t_menu (f_menu_id,f_menu_name,f_url,f_parentid,f_display,f_index,f_isdelete,f_menu_level)value(12,'服务器信息','',1,1,1,1,1);

#
# structure for table "t_tablequery"
#
drop table if exists `t_table_query`;
create table `t_table_query` # 菜单报表管理
(
	`f_item_id` 				int(11) 				not null 		auto_increment 	comment '自增长,作为表主键',
	`f_menu_id` 				int(11) 				default	null 					comment '菜单id',
	`f_table_name` 				varchar(50) 			default null 					comment '列表表名/视图',
	`f_table_form_name` 		varchar(50) 			default null 					comment '表单表名',
	`f_description` 			varchar(50) 			default null 					comment '描述',
	`f_orderby` 				varchar(255) 			default	null 					comment '排序字段',
	`f_entry_height` 			int(5)					default	null 					comment '表格高度',
	`f_display` 				int(1) 					default null 					comment '是否可见',
	primary key (`f_item_id`)
) engine=innodb default charset=utf8 comment='菜单报表管理'; 

#
# structure for table "t_conditions"
#
drop table if exists `t_conditions`;
create table `t_conditions` # 报表条件管理
(
	`t_table_query_id` 			int(11) 				not null 		 				comment '菜单报表ID',
	`f_field_name` 				varchar(11) 			default	null 					comment '字段',
	`f_display_name` 			varchar(50) 			default null 					comment '别名',
	`f_controls` 				int(1) 					default null 					comment 'input类型',
	`f_link_id` 				varchar(50) 			default null 					comment '连接ID 关联到某张表映射的id 例(弹出或关联出入省市县)',
	`f_link_key` 				varchar(50) 			default null 					comment '连接显示值 文字的字段 例(北京市朝阳区)',
	`f_link_value` 				varchar(50) 			default null 					comment '连接id值 id的字段 例(100011)',
	`f_default_value` 			varchar(50) 			default	null 					comment '默认值',
	`f_index` 					int(9) 					not null 						comment '序号'
) engine=innodb default charset=utf8 comment='报表条件管理'; 



#
# structure for table "t_fields_list_desc"
#
drop table if exists `t_fields_list_desc`;
create table `t_fields_list_desc` # 列表管理
(
	`t_table_query_id` 			int(11) 					not null 		 			comment '菜单报表ID',
	`f_name` 					varchar(50) 				not null 					comment '字段',
	`f_description` 			varchar(50) 				default null 				comment '别名',
	`f_link_name` 				varchar(50) 				default null 				comment '链接字段',
	`f_visible` 				varchar(50) 				not null 					comment '是否显示',
	`f_datatype` 				varchar(255) 				default	null 				comment '数据类型',
	`f_default_value` 			varchar(50) 				default	null 				comment '默认值',
	`f_index` 					int(5) 						not null 					comment '序号',
	`f_align` 					int(5) 						not null 					comment '对齐方式',
	`f_width` 					int(5)						not	null 					comment '表格宽度',
	`f_dateformat` 				varchar(50)  				default null 				comment '日期格式化',
	`f_allow_sort` 				int(1)						default null 				comment '是否排序',
	`f_filter` 					varchar(50)  				default null 				comment '是否可见',
	`f_dict` 					int(5) 						default null 				comment '过滤条件框'
) engine=innodb default charset=utf8 comment='列表管理'; 


#
# structure for table "t_buttons_list_desc"
#
drop table if exists `t_buttons_list_desc`;
create table `t_buttons_list_desc` # 列表按钮管理
(
	`f_item_id` 				int(11)     			not null 		auto_increment 	comment '自增长,作为表主键',
	`f_table_query_id` 			varchar(50) 			not null 						comment '菜单报表ID',
	`f_btn_id`					varchar(50) 			not null 						comment '按键id名称  例如 btn_yfzxbll2_add',
	`f_description` 			varchar(50) 			default null 					comment '按键名称    例如 添加',
	`f_visible` 				int(1) 					not null                        comment '是否可见',
	`f_index` 					int(9) 					not null 						comment '序号',
	`f_width` 					int(9) 					default null 					comment '宽度',
	`f_content` 				varchar(2000) 			default null 					comment '嵌入的后台脚本',
	`f_img` 					varchar(50) 			default null 					comment '按钮图标',
	`f_click` 					varchar(200) 			default null 					comment '点击事件',
	`f_sqlstr` 					varchar(2000) 			default null 					comment '后台执行的sql',
	`f_success_txt` 			varchar(50) 			default null 					comment '成功后提示',
	`f_fails_txt` 				varchar(50) 			default null 					comment '失败后提示',
	`f_role` 					int(9) 					default null 					comment '按钮权限id',
	primary key (`f_item_id`)
) engine=innodb default charset=utf8 comment='列表按钮管理'; 




#
# structure for table "t_fields_form_desc"
#
drop table if exists `t_fields_form_desc`;
create table `t_fields_form_desc` # 表单管理
(
	`t_table_query_id` 			int(11) 					not null 		 			comment '菜单报表ID',
	`f_name` 					varchar(50) 				not null 					comment '字段',
	`f_description` 			varchar(50) 				default null 				comment '别名',
	`f_link_id` 				varchar(50) 				default null 				comment '连接ID 关联到某张表映射的id 例(弹出或关联出入省市县)',
	`f_link_key` 				varchar(50) 				default null 				comment '连接显示值 文字的字段 例(北京市朝阳区)',
	`f_link_value` 				varchar(50) 				default null 				comment '连接id值 id的字段 例(100011)',
	`f_visible` 				varchar(50) 				not null 					comment '是否显示',
	`f_data_type` 				varchar(255) 				default	null 				comment '数据类型',
	`f_default_value` 			varchar(50) 				default	null 				comment '默认值',
	`f_colspan` 				int(5) 						default	null 				comment '跨列数',
	`f_required` 				int(1) 						default	0 					comment '是否必填',
	`f_readonly` 				int(1) 						default	0 					comment '是否只读',
	`f_controls` 				int(1) 						default	0 					comment 'input类型',
	`f_label_css` 				varchar(50) 				default	null 				comment '标签样式',
	`f_value_css` 				varchar(50) 				default	null 				comment 'input样式',
	`f_attrs` 					varchar(50) 				default	null 				comment '属性',
	`f_is_buttom` 				int(1) 						default	0 					comment '是否底部',
	`f_rule` 					varchar(50) 				default	null 				comment '验证规则',
	`f_chain_sql` 				varchar(50) 				default	null 				comment '级联的数据sql',
	`f_index` 					int(5) 						not null 					comment '序号'
) engine=innodb default charset=utf8 comment='表单管理'; 



#
# structure for table "t_database_list"
#
drop table if exists `t_database_list`;
create table `t_database_list` # 远程数据库信息
(
	`f_id` 						bigint     				not null 		auto_increment 	comment '自增长,作为表主键',
	`f_type` 					tinyint(5) 				not null 						comment '数据库类型',
	`f_title` 					varchar(50) 			not null 						comment '数据库别名',
	`f_ip`						varchar(50)				not null						comment '数据库IP',
	`f_port`					int(6)					not null						comment '数据库端口号',
	`f_db_name`					varchar(50)				not null						comment '数据库名',
	`f_login_name`				varchar(50)				not null						comment '登录名',
	`f_login_password`			varchar(50)				not null						comment '登录密码',
	`f_index` 					int(9) 					default null 					comment '序号',
	`f_is_delete` 				tinyint(3) 				default 0 						comment '是否删除',
	`f_description` 			varchar(50) 			default null 					comment '描述',
	primary key (`f_id`)
) engine=innodb default charset=utf8 comment='远程数据库信息'; 

#
# structure for table "data_material_list"
#
drop table if exists `data_material_list`;
create table `data_material_list` # 数据库常用资料表
(
	`id` 						bigint     				not null 		auto_increment 	comment '自增长,作为表主键',
	`name` 						varchar(50) 			not null 						comment '名称',
	`parentid`					bigint 					default 0 						comment '父ID',
	`description` 				varchar(200) 			default null 					comment '描述',
	`index` 					int(9) 					default null 					comment '序号',
	`is_delete` 				tinyint(3) 				default 0 						comment '是否删除',
	primary key (`id`)
) engine=innodb default charset=utf8 comment='数据库常用资料表'; 
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(1,'数据库类型',0,'数据库的类型 oracle mysql 等',1,0);
-- 数据库类型
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(2,'mySql',1,'mysql数据库',1,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(3,'oracle',1,'oracle数据库',2,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(4,'sqlServer',1,'sqlserver数据库',3,0);
-- mysql 数据库类型
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(5,'tinyint',2,'小整数值',1,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(6,'smallint',2,'大整数值',2,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(7,'mediumint',2,'大整数值',3,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(8,'int',2,'大整数值',4,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(9,'bigint',2,'极大整数值',5,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(10,'float',2,'单精度',6,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(11,'double',2,'双精度',7,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(12,'decimal',2,'小数值',8,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(13,'date',2,'日期值',9,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(14,'time',2,'时间值或持续时间',10,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(15,'year',2,'年份值',11,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(16,'datetime',2,'混合日期和时间值',12,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(17,'timestamp',2,'混合日期和时间值，时间戳',13,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(18,'char',2,'定长字符串',14,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(19,'varchar',2,'变长字符串',15,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(20,'tinyblob',2,'不超过 255 个字符的二进制字符串',16,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(21,'tinytext',2,'短文本字符串',17,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(22,'blob',2,'二进制形式的长文本数据',18,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(23,'text',2,'长文本数据',19,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(24,'mediumblob',2,'二进制形式的中等长度文本数据',20,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(25,'mediumtext',2,'中等长度文本数据',21,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(26,'longblob',2,'二进制形式的极大文本数据',22,0);
insert into data_material_list (f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(27,'longtext',2,'极大文本数据',23,0);
-- oracle 数据库类型
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(28,'char',3,'固定长度字符串最大长度2000bytes',1,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(29,'varchar2',3,'可变长度的字符串最大长度4000bytes可做索引的最大长度749',2,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(30,'nchar',3,'根据字符集而定的固定长度字符串最大长度2000bytes',3,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(31,'nvarchar2',3,'根据字符集而定的可变长度字符串最大长度4000bytes',4,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(32,'date',3,'日期（日-月-年）dd-mm-yy（hh-mi-ss）经过严格测试，无千虫问题',5,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(33,'long',3,'超长字符串最大长度2g（231-1）足够存储大部头著作',6,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(34,'raw',3,'固定长度的二进制数据最大长度2000bytes可存放多媒体图象声音等',7,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(35,'longraw',3,'可变长度的二进制数据最大长度2g同上',8,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(36,'blob',3,'二进制数据最大长度4g',9,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(37,'clob',3,'字符数据最大长度4g',10,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(38,'nclob',3,'根据字符集而定的字符数据最大长度4g',11,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(39,'bfile',3,'存放在数据库外的二进制数据最大长度4g',12,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(40,'rowid',3,'数据表中记录的唯一行号10bytes********.****.****格式，*为0或1',13,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(41,'nrowid',3,'二进制数据表中记录的唯一行号最大长度4000bytes',14,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(42,'number(p,s)',3,'数字类型p为整数位，s为小数位',15,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(43,'decimal(p,s)',3,'数字类型p为整数位，s为小数位',16,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(44,'integer',3,'整数类型小的整数',17,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(45,'float',3,'浮点数类型number(38)，双精度',18,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(46,'real',3,'实数类型number(63)，精度更高',19,0);
-- sqlserver 数据库类型
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(47,'bit',4,'数据类型是整型，其值只能是0、1或空值。这种数据类型用于存储只有两种可能值的数据，如Yes或No、True或False、On或Off.',1,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(48,'tinyint',4,'数据类型能存储从0到255之间的整数。它在你只打算存储有限数目的数值时很有用。这种数据类型在数据库中占用1个字节.',2,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(49,'smallint',4,'数据类型可以存储从-2的15次幂(-32768)到2的15次幂(32767)之间的整数。这种数据类型对存储一些常限定在特定范围内的数值型数据非常有用。这种数据类型在数据库里占用2字节空间.',3,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(50,'int',4,'数据类型可以存储从-2的31次幂(-2147483648)到2的31次幂(2147483647)之间的整数。存储到数据库的几乎所有数值型的数据都可以用这种数据类型。这种数据类型在数据库里占用4个字节.',4,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(51,'decimal',4,'数据类型能用来存储从-10的38次幂-1到10的38次幂-1的固定精度和范围的数值型数据。使用这种数据类型时，必须指定范围和精度。范围是小数点左右所能存储的数字的总位数。精度是小数点右边存储的数字的位数',5,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(52,'numeric',4,'数据类型与decimal相似。',6,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(53,'smallmoney',4,'数据类型用来表示钱和货币值。这种数据类型能存储从-214748.3648到214748.3647之间的数据，精确到货币单位的万分之一',7,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(54,'money',4,'数据类型用来表示钱和货币值。这种数据类型能存储从-9220亿到9220亿之间的数据，精确到货币单位的万分之一',8,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(55,'float',4,'数据类型是一种近似数值类型，供浮点数使用。说浮点数是近似的，是因为在其范围内不是所有的数都能精确表示。浮点数可以是从-1.79E+308到1.79E+308之间的任意数',9,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(56,'real',4,'数据类型像浮点数一样，是近似数值类型。它可以表示数值在-3.40E+38到3.40E+38之间的浮点数',10,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(57,'Smalldatetime',4,'数据类型用来表示从1900年1月1日到2079年6月6日间的日期和时间，精确到一分钟',11,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(58,'datetime',4,'数据类型用来表示日期和时间。这种数据类型存储从1753年1月1日到9999年12月31日间所有的日期和时间数据，精确到三百分之一秒或3.33毫秒.',12,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(59,'cursor',4,'数据类型是一种特殊的数据类型，它包含一个对游标的引用。这种数据类型用在存储过程中，而且创建表时不能用',13,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(60,'timestamp',4,'数据类型是一种特殊的数据类型，用来创建一个数据库范围内的唯一数码。一个表中只能有一个timestamp列。每次插入或修改一行时，timestamp列的值都会改变。尽管它的名字中有“time”，但timestamp列不是人们可识别的日期。在一个数据库里，timestamp值是唯一的',14,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(61,'Uniqueidentifier',4,'特殊数据型数据类型用来存储一个全局唯一标识符，即GUID。GUID确实是全局唯一的。这个数几乎没有机会在另一个系统中被重建。可以使用NEWID函数或转换一个字符串为唯一标识符来初始化具有唯一标识符的列',15,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(62,'char',4,'数据类型用来存储指定长度的定长非统一编码型的数据。当定义一列为此类型时，你必须指定列长。当你总能知道要存储的数据的长度时，此数据类型很有用。例如，当你按邮政编码加4个字符格式来存储数据时，你知道总要用到10个字符。此数据类型的列宽最大为8000个字符.',16,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(63,'varcharvarchar',4,'数据类型，同char类型一样，用来存储非统一编码型字符数据。与char型不一样，此数据类型为变长。当定义一列为该数据类型时，你要指定该列的最大长度。它与char数据类型最大的区别是，存储的长度不是列长，而是数据的长度.',17,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(64,'text',4,'数据类型用来存储大量的非统一编码型字符数据。这种数据类型最多可以有231-1或20亿个字符.',18,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(65,'nchar',4,'数据类型用来存储定长统一编码字符型数据。统一编码用双字节结构来存储每个字符，而不是用单字节(普通文本中的情况)。它允许大量的扩展字符。此数据类型能存储4000种字符，使用的字节空间上增加了一倍.',19,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(66,'nvarchar',4,'数据类型用作变长的统一编码字符型数据。此数据类型能存储4000种字符，使用的字节空间增加了一倍.',20,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(67,'ntext',4,'数据类型用来存储大量的统一编码字符型数据。这种数据类型能存储230-1或将近10亿个字符，且使用的字节空间增加了一倍',21,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(68,'binary',4,'数据类型用来存储可达8000字节长的定长的二进制数据。当输入表的内容接近相同的长度时，你应该使用这种数据类型.',22,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(69,'varbinary',4,'数据类型用来存储可达8000字节长的变长的二进制数据。当输入表的内容大小可变时，你应该使用这种数据类型',23,0);
insert into data_material_list(f_id,f_name,f_parentid,f_description,f_index,f_is_delete)value(70,'image',4,'数据类型用来存储变长的二进制数据，最大可达231-1或大约20亿字节',24,0);




#
# structure for table "t_database_tables"
#
drop table if exists `t_database_tables`;
create table `t_database_tables` # 数据库表
(
	`f_table_name` 				varchar(100) 			not null 						comment '表名',
	`f_comment` 				varchar(100) 			not null 						comment '表别名',
	`f_num_rows`				int(6)					not null						comment '数据行数',
	primary key (`f_table_name`)
) engine=innodb default charset=utf8 comment='数据库表'; 


#
# structure for table "t_table_columns"
#
drop table if exists `t_table_columns`;
create table `t_table_columns` # 数据库表字段
(
	`f_table_name` 				varchar(100) 			not null 						comment '表名',
	`f_name` 					varchar(100) 			not null 						comment '列名',
	`f_comment` 				varchar(100) 			not null 						comment '列别名',
	`f_data_type`				int(6)					not null						comment '数据类型',
	`f_data_length`				int(5)					not null						comment '数据长度',
	`f_data_precision`			int(6)					default null					comment '数据精度',
	`f_nullable`				tinyint(2)				default 0						comment '数据不可空',
	`f_data_default`			varchar(100)			default null					comment '默认值',
	`f_data_constraint_type`	varchar(5)				default null					comment '约束',
	primary key (`f_table_name`,`f_name`)
) engine=innodb default charset=utf8 comment='数据库表字段'; 




/**
create table t_replace_value
(
	f_id number(18),
	f_replace_key VARCHAR2(50),
	f_isvalue int,
	f_value VARCHAR2(200),
	f_sql_value VARCHAR2(200)
)
**/


