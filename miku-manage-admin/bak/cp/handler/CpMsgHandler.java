package com.fly.miku.modules.wechat.modules.cp.handler;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.fly.miku.modules.wechat.common.utils.JsonUtils;
import com.fly.miku.modules.wechat.modules.cp.builder.CpTextBuilder;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.bean.WxCpXmlMessage;
import me.chanjar.weixin.cp.bean.WxCpXmlOutMessage;

/**
 * * @author Binary Wang(https://github.com/binarywang)
 */
@Component
public class CpMsgHandler extends CpAbstractHandler {

  @Override
  public WxCpXmlOutMessage handle(WxCpXmlMessage wxMessage,
                                  Map<String, Object> context, WxCpService weixinService,
                                  WxSessionManager sessionManager) {

    if (!wxMessage.getMsgType().equals(WxConsts.XmlMsgType.EVENT)) {
      //TODO 可以选择将消息保存到本地
    }

    //TODO 组装回复消息
    String content = "收到信息内容：" + JsonUtils.toJson(wxMessage);

    return new CpTextBuilder().build(content, wxMessage, weixinService);

  }

}
