package com.fly.miku.modules.wechat.modules.cp.handler;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.fly.miku.modules.wechat.common.utils.JsonUtils;

import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.bean.WxCpXmlMessage;
import me.chanjar.weixin.cp.bean.WxCpXmlOutMessage;

/**
 * * @author Binary Wang(https://github.com/binarywang)
 */
@Component
public class CpLogHandler extends CpAbstractHandler {
  @Override
  public WxCpXmlOutMessage handle(WxCpXmlMessage wxMessage,
                                  Map<String, Object> context, WxCpService WxCpService,
                                  WxSessionManager sessionManager) {
    this.logger.info("\n接收到请求消息，内容：{}", JsonUtils.toJson(wxMessage));
    return null;
  }

}
