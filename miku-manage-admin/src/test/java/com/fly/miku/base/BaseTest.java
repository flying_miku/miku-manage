package com.fly.miku.base;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.fly.miku.start.ProgramStart;

/**
 * 命名规则：
 *    1. 测试类类名： 测试的基类后添加Test  例如, UserController 测试类名为 UserControllerTest
 *    2. 测试方法名： 测试的基类的方法名前添加test 例如, add() 测试名为 testAdd().
 */
@RunWith(SpringRunner.class) // SpringJUnit支持，由此引入Spring-Test框架支持！
@SpringBootTest(classes=ProgramStart.class)
@Rollback(true)
@Transactional(rollbackFor=Exception.class)
public class BaseTest {
}
