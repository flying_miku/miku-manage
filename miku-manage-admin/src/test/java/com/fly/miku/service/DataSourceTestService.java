package com.fly.miku.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fly.miku.datasources.DataSourceNames;
import com.fly.miku.datasources.annotation.DataSource;
import com.fly.miku.modules.sys.entity.SysUserEntity;
import com.fly.miku.modules.sys.service.SysUserService;

/**
 * 测试多数据源
 *
 * @author Flying flying_miku@sina.com
 * @since 3.1.0 2018-01-28
 */
@Service
public class DataSourceTestService {
    @Autowired
    private SysUserService sysUserService;

    public SysUserEntity queryUser(Long userId){
        return sysUserService.selectById(userId);
    }

    @DataSource(name = DataSourceNames.SECOND)
    public SysUserEntity queryUser2(Long userId){
        return sysUserService.selectById(userId);
    }
}
