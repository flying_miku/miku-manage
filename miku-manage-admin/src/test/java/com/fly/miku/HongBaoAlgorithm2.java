package com.fly.miku;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;


public class HongBaoAlgorithm2 {
	static Random random = new Random();
	static {
		random.setSeed(System.currentTimeMillis());
	}

	public static void main(String[] args) {
		
	
		
		/*****************************************************************
		 * 金额300000元   数量 100000个   平均值为3元 
		 ****************************************************************/
		BigDecimal amtBd2 = new BigDecimal(0.8).setScale(1,BigDecimal.ROUND_DOWN);
				System.out.println(amtBd2);
		long amt = 30_0000_000;
		BigDecimal amtBd = new BigDecimal(amt).setScale(0);
		int sumCount =  10_0000;
		BigDecimal sumCountBd = new BigDecimal(sumCount).setScale(0);
		
		//最大值   
		BigDecimal max = new BigDecimal(1000_00).setScale(0);
		//最小值
		BigDecimal min = new BigDecimal(1).setScale(0);
		
		int exponent = ((int)Math.log10(amtBd.doubleValue()))-5;
		//最大档  最大值
		BigDecimal exponentMax = exponent<=3?new BigDecimal(Math.pow(10,exponent)).setScale(0):max;

		//最大数量
		int maxCount = amtBd.divide(max).divide(exponentMax,0,BigDecimal.ROUND_DOWN).intValue();
		
		
		System.out.println(maxCount);
		System.out.println(exponentMax);
		
		
		
		
		
		//amtBd.longValue()
		
		
		//List<Map<Integer,Integer>> resList = new ArrayList<Map<Integer,Integer>>();
		
		/*LinkedHashMap<Integer,Map<Integer,BigDecimal>> res1= new LinkedHashMap<Integer,Map<Integer,BigDecimal>>();
		Map<Integer,BigDecimal> map1= new HashMap<Integer,BigDecimal>();
		map1.put(49,new BigDecimal( 0.27).setScale(5, BigDecimal.ROUND_HALF_DOWN));
		res1.put(10, map1);
		 map1= new HashMap<Integer,BigDecimal>();
		 map1.put(99,new BigDecimal(0.30).setScale(5, BigDecimal.ROUND_HALF_DOWN));
		res1.put(50, map1);
		 map1= new HashMap<Integer,BigDecimal>();
		 map1.put(149, new BigDecimal(0.09).setScale(5, BigDecimal.ROUND_HALF_DOWN));
		res1.put(100, map1);
		 map1= new HashMap<Integer,BigDecimal>();
		 map1.put(299,new BigDecimal( 0.09).setScale(5, BigDecimal.ROUND_HALF_DOWN));
		res1.put(150, map1);
		 map1= new HashMap<Integer,BigDecimal>();
		 map1.put(599,new BigDecimal(0.12).setScale(5, BigDecimal.ROUND_HALF_DOWN));
		res1.put(300, map1);
		 map1= new HashMap<Integer,BigDecimal>();
		 map1.put(1199,new BigDecimal(0.04669).setScale(5, BigDecimal.ROUND_HALF_DOWN));
		res1.put(600, map1);
		 map1= new HashMap<Integer,BigDecimal>();
		 map1.put(2399, new BigDecimal(0.04).setScale(5, BigDecimal.ROUND_HALF_DOWN));
		res1.put(1200, map1);
		 map1= new HashMap<Integer,BigDecimal>();
		 map1.put(4799, new BigDecimal(0.04).setScale(5, BigDecimal.ROUND_HALF_DOWN));
		res1.put(2400, map1);
		 map1= new HashMap<Integer,BigDecimal>();
		 map1.put(9599,new BigDecimal(0.0013).setScale(5, BigDecimal.ROUND_HALF_DOWN));
		res1.put(4800, map1);
		 map1= new HashMap<Integer,BigDecimal>();
		 map1.put(18000, new BigDecimal(0.002).setScale(5, BigDecimal.ROUND_HALF_DOWN));
		res1.put(9600, map1);
		 map1= new HashMap<Integer,BigDecimal>();
		 map1.put(100000,new BigDecimal(0.00001).setScale(5, BigDecimal.ROUND_HALF_DOWN));
		res1.put(100000, map1);
		
		 BigDecimal sumMin = new BigDecimal(0);
		 BigDecimal sumMXn = new BigDecimal(0);
		LinkedHashMap<BigDecimal,BigDecimal> sumLink = new LinkedHashMap<BigDecimal,BigDecimal>(); 
		for (Integer min : res1.keySet()) {
			Map<Integer,BigDecimal> map = res1.get(min);
			for(Integer max : map.keySet()){
				BigDecimal rate= map.get(max); 
				BigDecimal minBd = new BigDecimal(min);
				BigDecimal maxBd = new BigDecimal(max);
				BigDecimal tempSumMin = minBd.multiply(rate.multiply(sumCountBd));
				BigDecimal tempSumMax = maxBd.multiply(rate.multiply(sumCountBd));
				sumLink.put(tempSumMin , tempSumMax);
				sumMin=sumMin.add(tempSumMin);
				sumMXn=sumMXn.add(tempSumMax);
			}
		}
		
		BigDecimal sumAmt = new BigDecimal(0);
		BigDecimal rate=sumMin.divide(amtBd,9,BigDecimal.ROUND_UP);
		for(BigDecimal min : sumLink.keySet()){
			BigDecimal bigDecimal = min.divide(rate,0,BigDecimal.ROUND_HALF_UP);
			sumAmt=sumAmt.add(bigDecimal);
			System.out.println(bigDecimal);
		}
		if(amtBd.subtract(sumMin).doubleValue()>0&&sumMXn.subtract(amtBd).doubleValue()>0){
			if(sumAmt.subtract(amtBd).abs().doubleValue()<1){
				
			}else{
				System.out.println("比例配置不精确");
			}
		}else{
			System.out.println("数据不在区间范围内");
		}
		
		System.out.println(sumAmt);*/
		
		
		
		
		/*	Long[] result = generate(2106412,200 ,18000,9600);
			double total = 0;
			for (int i = 0; i < result.length; i++) {
				// System.ou+t.println("result[" + i + "]:" + result[i]);
				// System.out.println(result[i]);
				System.out.println(result[i]);
				total += (result[i]);
			}
			System.out.println("total:" + total);
		System.out.println("打乱前");
		List<Long>list1= Arrays.asList(result);
		System.out.println(list1);	
		System.out.println("打乱后");
		//打乱list顺序
		Collections.shuffle(list1);
		System.out.println(list1);	
		
		*/
		/*
		long max = 18000;
		long min = 9610;
		long totalMoney = 50000;
		int totalNum = 5;

		Long[] result = generate(totalMoney, totalNum, max, min);
		double total = 0;
		for (int i = 0; i < result.length; i++) {
			// System.out.println("result[" + i + "]:" + result[i]);
			// System.out.println(result[i]);
			System.out.println(result[i]);
			total += (result[i]);
		}
		System.out.println("打乱前");
		List<Long>list1= Arrays.asList(result);
		System.out.println(list1);	
		System.out.println("打乱后");
		//打乱list顺序
		Collections.shuffle(list1);
		System.out.println(list1);	
		//检查生成的红包的总额是否正确
		System.out.println("total:" + total);

		//统计每个钱数的红包数量，检查是否接近正态分布
		int count[] = new int[(int) max + 1];
		for (int i = 0; i < result.length; i++) {
			count[(int) result[i]] += 1;
		}

		for (int i = 0; i < count.length; i++) {
			System.out.println("" + i + "  " + count[i]);
		}*/
	}

	/**
	 * 生产min和max之间的随机数，但是概率不是平均的，从min到max方向概率逐渐加大。
	 * 先平方，然后产生一个平方值范围内的随机数，再开方，这样就产生了一种“膨胀”再“收缩”的效果。
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	static long xRandom(long min, long max) {
		return sqrt(nextLong(sqr(max - min)));
	}

	/**
	 * 
	 * @param total
	 *            红包总额
	 * @param count
	 *            红包个数
	 * @param max
	 *            每个小红包的最大额
	 * @param min
	 *            每个小红包的最小额
	 * @return 存放生成的每个小红包的值的数组
	 */
	public static Long[] generate(long total, int count, long max, long min) {
		Long[] result = new Long[count];

		long average = total / count;

		long a = average - min;
		long b = max - min;

		//
		// 这样的随机数的概率实际改变了，产生大数的可能性要比产生小数的概率要小。
		// 这样就实现了大部分红包的值在平均数附近。大红包和小红包比较少。
		long range1 = sqr(average - min);
		long range2 = sqr(max - average);

		for (int i = 0; i < result.length; i++) {
			// 因为小红包的数量通常是要比大红包的数量要多的，因为这里的概率要调换过来。
			// 当随机数>平均值，则产生小红包
			// 当随机数<平均值，则产生大红包
			if (nextLong(min, max) > average) {
				// 在平均线上减钱
				// long temp = min + sqrt(nextLong(range1));
				long temp = min + xRandom(min, average);
				result[i] = temp;
				total -= temp;
			} else {
				// 在平均线上加钱
				// long temp = max - sqrt(nextLong(range2));
				long temp = max - xRandom(average, max);
				result[i] = temp;
				total -= temp;
			}
		}
		// 如果还有余钱，则尝试加到小红包里，如果加不进去，则尝试下一个。
		while (total > 0) {
			for (int i = 0; i < result.length; i++) {
				if (total > 0 && result[i] < max) {
					result[i]++;
					total--;
				}
			}
		}
		// 如果钱是负数了，还得从已生成的小红包中抽取回来
		while (total < 0) {
			for (int i = 0; i < result.length; i++) {
				if (total < 0 && result[i] > min) {
					result[i]--;
					total++;
				}
			}
		}
		return result;
	}

	static long sqrt(long n) {
		// 改进为查表？
		return (long) Math.sqrt(n);
	}

	static long sqr(long n) {
		// 查表快，还是直接算快？
		return n * n;
	}

	static long nextLong(long n) {
		return random.nextInt((int) n);
	}

	static long nextLong(long min, long max) {
		return random.nextInt((int) (max - min + 1)) + min;
	}
}
