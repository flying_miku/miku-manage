package com.fly.miku;

import org.apache.poi.hssf.util.HSSFColor;

import com.fly.miku.common.excel.annotation.ExcelField;
import com.fly.miku.common.excel.annotation.ExcelSheet;

import lombok.Data;

@Data
@ExcelSheet(name = "商户列表", headColor = HSSFColor.HSSFColorPredefined.LIGHT_GREEN)
public class ShopDTO {

    @ExcelField(name = "商户ID")
    private int shopId;

    @ExcelField(name = "商户名称")
    private String shopName;
    



}
