package com.fly.miku;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName: MakeOrderNum
 * @CreateTime 2015年9月13日 下午4:51:02
 * @author : mayi
 * @Description: 订单号生成工具，生成非重复订单号，理论上限1毫秒1000个，可扩展
 *
 */
public class MakeOrderNum {
	/**
	 * 锁对象，可以为任意对象
	 */
	private static Object lockObj = "lockerOrder";
	/**
	 * 订单号生成计数器
	 */
	private static long orderNumCount = 0L;
	/**
	 * 每毫秒生成订单号数量最大值
	 */
	private static int maxPerMSECSize=1000;
	/**
	 * 生成非重复订单号，理论上限1毫秒1000个，可扩展
	 * @param tname 测试用
	 */
	
	private final static SimpleDateFormat SIMPLEDATEFORMAT =  new SimpleDateFormat("yyyyMMddHHmmssSSS"); 
	
	
	
	public static String makeOrderNum(Date date) {
			// 最终生成的订单号
			synchronized (lockObj) {
			
				// 计数器到最大值归零，可扩展更大，目前1毫秒处理峰值1000个，1秒100万
				if (orderNumCount >= maxPerMSECSize) {
					orderNumCount = 0L;
				}
				// 取系统当前时间作为订单号变量前半部分，精确到毫秒
				long nowLong = Long.parseLong(SIMPLEDATEFORMAT.format(date));
				//组装订单号
				return nowLong+(maxPerMSECSize +(orderNumCount++)+"").substring(1);
				// Thread.sleep(1000);
			}
	}
	
	
	public static String makeOrderNum(String type) {
		// 最终生成的订单号
		synchronized (lockObj) {
			// 取系统当前时间作为订单号变量前半部分，精确到毫秒
			// 计数器到最大值归零，可扩展更大，目前1毫秒处理峰值1000个，1秒100万
			if (orderNumCount >= maxPerMSECSize) {
				orderNumCount = 0L;
			}
			// 取系统当前时间作为订单号变量前半部分，精确到毫秒
			long nowLong = Long.parseLong(SIMPLEDATEFORMAT.format(new Date()));
			//组装订单号
			return type+nowLong+(maxPerMSECSize +(orderNumCount++)+"").substring(1);
			// Thread.sleep(1000);
			// Thread.sleep(1000);
		}
}

	public static void main(String[] args) {
		// 测试多线程调用订单号生成工具
		Long startTime1 = System.currentTimeMillis();
		for(int i=0;i<1;i++){
			System.out.println(MakeOrderNum.makeOrderNum("A"));
		}
		Long endTime1 = System.currentTimeMillis();
		
		
		Long  startTime2 = System.currentTimeMillis();
		for(int i=0;i<1000;i++){
			MakeOrderNum.makeOrderNum(new Date());
		}
		Long endTime2 = System.currentTimeMillis();
		
		
	/*	for (String string : order) {
			System.out.println(string);
		}*/
		System.out.println((endTime1-startTime1));
		System.out.println((endTime2-startTime2));
	}

}
