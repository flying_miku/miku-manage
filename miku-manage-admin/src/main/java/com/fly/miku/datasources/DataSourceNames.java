package com.fly.miku.datasources;

/**
 * 增加多数据源，在此配置
 *
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017/8/18 23:46
 */
public interface DataSourceNames {

    String FIRST = "first";
    String SECOND = "second";

}
