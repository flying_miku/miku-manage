package com.fly.miku.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.common.validator.ValidatorUtils;
import com.fly.miku.modules.sys.entity.SysDictEntity;
import com.fly.miku.modules.sys.service.SysDictService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 数据字典
 *
 * @author Flying flying_miku@sina.com
 * @since 3.1.0 2018-01-27
 */
@RestController
@RequestMapping(value="sys/dict")
@Api(tags = "数据字典API")
public class SysDictController {
    @Autowired
    private SysDictService sysDictService;

    /**
     * 列表
     */
    @RequestMapping(value="/list",method=RequestMethod.GET)
    @RequiresPermissions("sys:dict:list")
	@ApiOperation("列表")
    public ResultWrapper<PageUtils> list(@RequestParam Map<String, Object> params){
        PageUtils page = sysDictService.queryPage(params);
        return new ResultWrapper<PageUtils>(RespEnum.SUCCESS,page);
    }


    /**
     * 信息
     */
    @RequestMapping(value="/info/{id}",method=RequestMethod.GET)
    @RequiresPermissions("sys:dict:info")
    @ApiOperation("信息")
    public ResultWrapper<SysDictEntity> info(@PathVariable("id") Long id){
        SysDictEntity dict = sysDictService.selectById(id);
		return new ResultWrapper<SysDictEntity>(RespEnum.SUCCESS,dict);
    }

    /**
     * 保存
     */
    @RequestMapping(value="/save",method=RequestMethod.POST,
            consumes = {"application/json", "application/xml"})
    @RequiresPermissions("sys:dict:save")
    @ApiOperation("保存")
    public ResultWrapper<Boolean>  save(@RequestBody SysDictEntity dict){
        //校验类型
        ValidatorUtils.validateEntity(dict);
        sysDictService.insert(dict);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
    }

    /**
     * 修改
     */
    @RequestMapping(value="/update",method=RequestMethod.POST,
            consumes = {"application/json", "application/xml"})
    @RequiresPermissions("sys:dict:update")
    @ApiOperation("修改")
    public ResultWrapper<Boolean> update(@RequestBody SysDictEntity dict){
        //校验类型
        ValidatorUtils.validateEntity(dict);
        sysDictService.updateById(dict);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
    }

    /**
     * 删除
     */
    @RequestMapping(value="/delete",method=RequestMethod.POST,
            consumes = {"application/json", "application/xml"})
    @RequiresPermissions("sys:dict:delete")
    @ApiOperation("删除")
    public ResultWrapper<Boolean> delete(@RequestBody Long[] ids){
        sysDictService.deleteBatchIds(Arrays.asList(ids));
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
    }

}
