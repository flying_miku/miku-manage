package com.fly.miku.modules.api.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fly.miku.modules.api.entity.TokenEntity;

/**
 * 用户Token
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-03-23 15:22:07
 */
public interface TokenDao extends BaseMapper<TokenEntity> {
	
}
