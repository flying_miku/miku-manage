package com.fly.miku.modules.api.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2018-02-22 17:55:00
 */
@TableName("card_info")
public class CardInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 创建时间
	 */
	private Date creaDate;
	/**
	 * 正面图片
	 */
	private String positiveImage;
	/**
	 * 反面图片
	 */
	private String negativeImage;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：内容
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：内容
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreaDate(Date creaDate) {
		this.creaDate = creaDate;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreaDate() {
		return creaDate;
	}
	/**
	 * 设置：正面图片
	 */
	public void setPositiveImage(String positiveImage) {
		this.positiveImage = positiveImage;
	}
	/**
	 * 获取：正面图片
	 */
	public String getPositiveImage() {
		return positiveImage;
	}
	/**
	 * 设置：反面图片
	 */
	public void setNegativeImage(String negativeImage) {
		this.negativeImage = negativeImage;
	}
	/**
	 * 获取：反面图片
	 */
	public String getNegativeImage() {
		return negativeImage;
	}
}
