package com.fly.miku.modules.sys.entity;

import org.hibernate.validator.constraints.NotBlank;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 系统配置信息
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年12月4日 下午6:43:36
 */
@TableName("sys_config")
@Data
@ApiModel("系统配置信息")
public class SysConfigEntity {
	@TableId
	@ApiModelProperty(value="id")
	private Long id;
	
	@NotBlank(message="参数名不能为空")
	@ApiModelProperty(value="参数名")
	private String key;
	
	@NotBlank(message="参数值不能为空")
	@ApiModelProperty(value="参数值")
	private String value; 
	
	@ApiModelProperty(value="备注")
	private String remark;
	
}
