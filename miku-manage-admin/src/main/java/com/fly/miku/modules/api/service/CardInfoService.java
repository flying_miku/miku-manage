package com.fly.miku.modules.api.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.modules.api.entity.CardInfoEntity;

/**
 * 
 *
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2018-02-22 17:55:00
 */
public interface CardInfoService extends IService<CardInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

