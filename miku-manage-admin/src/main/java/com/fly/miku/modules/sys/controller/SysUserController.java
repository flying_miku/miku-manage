package com.fly.miku.modules.sys.controller;


import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fly.miku.common.annotation.SysLog;
import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.DateUtils;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.common.validator.Assert;
import com.fly.miku.common.validator.ValidatorUtils;
import com.fly.miku.common.validator.group.AddGroup;
import com.fly.miku.common.validator.group.UpdateGroup;
import com.fly.miku.modules.sys.entity.SysUserEntity;
import com.fly.miku.modules.sys.service.SysUserRoleService;
import com.fly.miku.modules.sys.service.SysUserService;
import com.fly.miku.modules.sys.shiro.ShiroUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 系统用户
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年10月31日 上午10:40:10
 */
@RestController
@RequestMapping(value="/sys/user")
@Api(tags = "系统用户API")
public class SysUserController extends AbstractController {
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysUserRoleService sysUserRoleService;
	
	/**
	 * 所有用户列表
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@RequiresPermissions("sys:user:list")
	@ApiOperation("所有用户列表")
	public ResultWrapper<PageUtils> list(@RequestParam Map<String, Object> params){
		PageUtils page = sysUserService.queryPage(params);
		return new ResultWrapper<PageUtils>(RespEnum.SUCCESS,page);
	}
	
	/**
	 * 获取登录的用户信息
	 */
	@RequestMapping(value="/info",method=RequestMethod.GET)
	@ApiOperation("获取登录的用户信息")
	public ResultWrapper<SysUserEntity> info(){
		return new ResultWrapper<SysUserEntity>(RespEnum.SUCCESS,getUser());
	}
	
	/**
	 * 修改登录用户密码
	 */
	@SysLog("修改密码")
	@RequestMapping(value="/password",method=RequestMethod.POST)
	@ApiOperation("修改密码")
	public ResultWrapper<Boolean> password(String password, String newPassword){
		Assert.isBlank(newPassword, "新密码不为能空");

		//原密码
		password = ShiroUtils.sha256(password, getUser().getSalt());
		//新密码
		newPassword = ShiroUtils.sha256(newPassword, getUser().getSalt());
				
		//更新密码
		boolean flag = sysUserService.updatePassword(getUserId(), password, newPassword);
		if(!flag){
			return new ResultWrapper<Boolean>(RespEnum.INCORRECT_ORIGINAL_PASSWORD);
		}
		
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 用户信息
	 */
	@RequestMapping(value="/info/{userId}",method=RequestMethod.GET)
	@RequiresPermissions("sys:user:info")
	@ApiOperation("用户信息")
	public ResultWrapper<SysUserEntity> info(@PathVariable("userId") Long userId){
		SysUserEntity user = sysUserService.selectById(userId);
		
		//获取用户所属的角色列表
		List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
		user.setRoleIdList(roleIdList);
		return new ResultWrapper<SysUserEntity>(RespEnum.SUCCESS,user);
	}
	
	/**
	 * 保存用户
	 */
	@SysLog("保存用户")
	@RequestMapping(value="/save",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:user:save")
	@ApiOperation("保存用户")
	public ResultWrapper<Boolean> save(@RequestBody SysUserEntity user){
		ValidatorUtils.validateEntity(user, AddGroup.class);
		sysUserService.save(user);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 修改用户
	 */
	@SysLog("修改用户")
	@RequestMapping(value="/update",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:user:update")
	@ApiOperation("修改用户")
	public ResultWrapper<Boolean> update(@RequestBody SysUserEntity user){
		ValidatorUtils.validateEntity(user, UpdateGroup.class);
		sysUserService.update(user);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 删除用户
	 */
	@SysLog("删除用户")
	@RequestMapping(value="/delete",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:user:delete")
	@ApiOperation("删除用户")
	public ResultWrapper<Boolean> delete(@RequestBody Long[] userIds){
		if(ArrayUtils.contains(userIds, 1L)){
			return new ResultWrapper<Boolean>(RespEnum.THE_SYSTEM_ADMINISTRATOR_CANT_DELETE);
		}
		if(ArrayUtils.contains(userIds, getUserId())){
			return new ResultWrapper<Boolean>(RespEnum.CURRENT_USER_CANNOT_DELETE);
		}
		sysUserService.deleteBatchIds(Arrays.asList(userIds));
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 
	 */
	@RequestMapping(value="/exportExcel",method=RequestMethod.GET)
	//@RequiresPermissions("sys:user:exportExcel")
	@ApiOperation("导出Excel")
	public void exportExcel(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, Object> params) throws IOException{
		byte[] data =sysUserService.exportExcel(params);
		response.reset();  
        response.setHeader("Content-Disposition", "attachment; filename=\"sysUser-"+DateUtils.formatDateTime(new Date())+".xls\"");  
        response.addHeader("Content-Length", "" + data.length);  
        response.setContentType("application/octet-stream; charset=UTF-8");  
        IOUtils.write(data, response.getOutputStream());  
	}
}
