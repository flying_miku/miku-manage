package com.fly.miku.modules.api.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.modules.api.annotation.Login;
import com.fly.miku.modules.api.annotation.LoginUser;
import com.fly.miku.modules.api.entity.UserEntity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 测试接口
 *
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-03-23 15:47
 */
@RestController
@RequestMapping("/api")
@Api(tags="测试接口")
public class ApiTestController {

    @Login
    @GetMapping("userInfo")
    @ApiOperation(value="获取用户信息", response=UserEntity.class)
    public ResultWrapper<UserEntity> userInfo(@ApiIgnore @LoginUser UserEntity user){
        return new ResultWrapper<UserEntity>(RespEnum.SUCCESS,user);
    }

    @Login
    @GetMapping("userId")
    @ApiOperation("获取用户ID")
    public ResultWrapper<Integer> userInfo(@ApiIgnore @RequestAttribute("userId") Integer userId){
        return new ResultWrapper<Integer>(RespEnum.SUCCESS,userId);

    }

    @GetMapping("notToken")
    @ApiOperation("忽略Token验证测试")
    public ResultWrapper<Boolean> notToken(){
    	
        return new ResultWrapper<Boolean>(RespEnum.SUCCESS,"无需token也能访问。。。");
    }

}
