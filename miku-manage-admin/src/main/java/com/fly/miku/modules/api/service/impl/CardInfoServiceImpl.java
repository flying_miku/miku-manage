package com.fly.miku.modules.api.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.Query;
import com.fly.miku.modules.api.dao.CardInfoDao;
import com.fly.miku.modules.api.entity.CardInfoEntity;
import com.fly.miku.modules.api.service.CardInfoService;


@Service("cardInfoService")
public class CardInfoServiceImpl extends ServiceImpl<CardInfoDao, CardInfoEntity> implements CardInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
    	String content = (String)params.get("content");
    	Wrapper<CardInfoEntity> wrapper = new EntityWrapper<CardInfoEntity>().like(StringUtils.isNotBlank(content),"content",content);
        Page<CardInfoEntity> page = this.selectPage(
                new Query<CardInfoEntity>(params).getPage(),
                wrapper 
        );
        page.setTotal(this.selectCount(wrapper));
        return new PageUtils(page);
    }

}
