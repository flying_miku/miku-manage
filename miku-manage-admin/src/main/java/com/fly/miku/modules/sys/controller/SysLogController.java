package com.fly.miku.modules.sys.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.modules.sys.service.SysLogService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * 系统日志
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-03-08 10:40:56
 */
@Controller
@RequestMapping(value="/sys/log")
@Api(tags = "系统日志API")
public class SysLogController {
	@Autowired
	private SysLogService sysLogService;
	
	/**
	 * 列表
	 */
	@ResponseBody
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@RequiresPermissions("sys:log:list")
	@ApiOperation("列表")
	public ResultWrapper<PageUtils> list(@RequestParam Map<String, Object> params){
		PageUtils page = sysLogService.queryPage(params);
        return new ResultWrapper<PageUtils>(RespEnum.SUCCESS,page);
	}
	
}
