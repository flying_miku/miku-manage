package com.fly.miku.modules.api.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fly.miku.modules.api.entity.CardInfoEntity;

/**
 * 
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2018-02-22 17:55:00
 */
public interface CardInfoDao extends BaseMapper<CardInfoEntity> {
	
}
