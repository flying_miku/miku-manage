package com.fly.miku.modules.sys.shiro;

import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fly.miku.modules.sys.service.SysMenuService;

import lombok.Getter;

/**
 * Shiro权限标签
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年12月3日 下午11:32:47
 */
@Component
public class ShiroTag {

	@Getter
	private String title;
	
	@Autowired
	private SysMenuService sysMenuService;
	
	
	/**
	 * 是否拥有该权限
	 * @param permission  权限标识
	 * @return   true：是     false：否
	 */
	public boolean hasPermission(String permission) {
		Subject subject = SecurityUtils.getSubject();
		return subject != null && subject.isPermitted(permission);
	}
	
	
	
	/**
	 * 获取菜单名称Title
	 * @param permission  权限标识
	 * @return   true：是     false：否
	 */
	public String  getTitle(Map map) { 
		Integer menuId =Integer.parseInt(map.get("menuId").toString()) ;
		this.title = sysMenuService.selectById(menuId).getName();
		return title;
	}

}
