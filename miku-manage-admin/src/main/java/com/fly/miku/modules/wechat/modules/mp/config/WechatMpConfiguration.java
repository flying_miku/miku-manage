package com.fly.miku.modules.wechat.modules.mp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fly.miku.modules.wechat.modules.mp.handler.MpAbstractHandler;
import com.fly.miku.modules.wechat.modules.mp.handler.MpImageHandler;
import com.fly.miku.modules.wechat.modules.mp.handler.MpKfSessionHandler;
import com.fly.miku.modules.wechat.modules.mp.handler.MpLocationHandler;
import com.fly.miku.modules.wechat.modules.mp.handler.MpLogHandler;
import com.fly.miku.modules.wechat.modules.mp.handler.MpMenuHandler;
import com.fly.miku.modules.wechat.modules.mp.handler.MpMsgHandler;
import com.fly.miku.modules.wechat.modules.mp.handler.MpNullHandler;
import com.fly.miku.modules.wechat.modules.mp.handler.MpStoreCheckNotifyHandler;
import com.fly.miku.modules.wechat.modules.mp.handler.MpSubscribeHandler;
import com.fly.miku.modules.wechat.modules.mp.handler.MpUnsubscribeHandler;
import com.fly.miku.modules.wechat.modules.mp.handler.MpVideoHandler;
import com.fly.miku.modules.wechat.modules.mp.handler.MpVoiceHandler;

import me.chanjar.weixin.common.api.WxConsts.EventType;
import me.chanjar.weixin.common.api.WxConsts.MenuButtonType;
import me.chanjar.weixin.common.api.WxConsts.XmlMsgType;
import me.chanjar.weixin.mp.api.WxMpConfigStorage;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.constant.WxMpEventConstants;

/**
 * wechat mp configuration
 *
 * @author Binary Wang(https://github.com/binarywang)
 */
@Configuration
@ConditionalOnClass(WxMpService.class)
@EnableConfigurationProperties(WechatMpProperties.class)
public class WechatMpConfiguration {
  @Autowired
  protected MpLogHandler logHandler;
  @Autowired
  protected MpNullHandler nullHandler;
  @Autowired
  protected MpKfSessionHandler kfSessionHandler;
  @Autowired
  protected MpStoreCheckNotifyHandler storeCheckNotifyHandler;
  @Autowired
  private WechatMpProperties properties;
  @Autowired
  private MpLocationHandler locationHandler;
  @Autowired
  private MpMenuHandler menuHandler;
  @Autowired
  private MpMsgHandler msgHandler;
  @Autowired
  private MpUnsubscribeHandler unsubscribeHandler;
  @Autowired
  private MpSubscribeHandler subscribeHandler;
//  @Autowired
//  private MpScanHandler scanHandler;
  @Autowired
  private MpImageHandler imageHandler;
  
  @Autowired
  private MpVideoHandler videoHandler;
  
  @Autowired
  private MpVoiceHandler voiceHandler;

  @Bean
  @ConditionalOnMissingBean
  public WxMpConfigStorage configStorage() {
    WxMpInMemoryConfigStorage configStorage = new WxMpInMemoryConfigStorage();
    configStorage.setAppId(this.properties.getAppId());
    configStorage.setSecret(this.properties.getSecret());
    configStorage.setToken(this.properties.getToken());
    configStorage.setAesKey(this.properties.getAesKey());
    return configStorage;
  }

  @Bean
  @ConditionalOnMissingBean
  public WxMpService wxMpService(WxMpConfigStorage configStorage) {
//        WxMpService wxMpService = new me.chanjar.weixin.mp.api.impl.okhttp.WxMpServiceImpl();
//        WxMpService wxMpService = new me.chanjar.weixin.mp.api.impl.jodd.WxMpServiceImpl();
//        WxMpService wxMpService = new me.chanjar.weixin.mp.api.impl.apache.WxMpServiceImpl();
    WxMpService wxMpService = new me.chanjar.weixin.mp.api.impl.WxMpServiceImpl();
    wxMpService.setWxMpConfigStorage(configStorage);
    return wxMpService;
  }

  @Bean
  public WxMpMessageRouter router(WxMpService wxMpService) {
    final WxMpMessageRouter newRouter = new WxMpMessageRouter(wxMpService);

    // 记录所有事件的日志 （异步执行）
    newRouter.rule().handler(this.logHandler).next();

    // 接收客服会话管理事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
        .event(WxMpEventConstants.CustomerService.KF_CREATE_SESSION)
        .handler(this.kfSessionHandler).end();
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
        .event(WxMpEventConstants.CustomerService.KF_CLOSE_SESSION)
        .handler(this.kfSessionHandler)
        .end();
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
        .event(WxMpEventConstants.CustomerService.KF_SWITCH_SESSION)
        .handler(this.kfSessionHandler).end();

    // 门店审核事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
        .event(WxMpEventConstants.POI_CHECK_NOTIFY)
        .handler(this.storeCheckNotifyHandler).end();

    // 自定义菜单事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
        .event(MenuButtonType.CLICK).handler(this.getMenuHandler()).end();

    // 点击菜单连接事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
        .event(MenuButtonType.VIEW).handler(this.nullHandler).end();

    // 关注事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
        .event(EventType.SUBSCRIBE).handler(this.getSubscribeHandler())
        .end();

    // 取消关注事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
        .event(EventType.UNSUBSCRIBE)
        .handler(this.getUnsubscribeHandler()).end();

    // 上报地理位置事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
        .event(EventType.LOCATION).handler(this.getLocationHandler())
        .end();

    // 扫码事件
   /* newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
        .event(EventType.SCAN).handler(this.getScanHandler()).end();*/
    
    // 接收地理位置消息
    newRouter.rule().async(false).msgType(XmlMsgType.LOCATION)
        .handler(this.getLocationHandler()).end();
    
    
    // 接收文字消息
    newRouter.rule().async(false).msgType(XmlMsgType.TEXT)
        .handler(this.getMsgHandler()).end();
    //@@@@@@@@@@@@
    // 接收文字消息
    newRouter.rule().async(false).msgType(XmlMsgType.IMAGE)
        .handler(this.getImageHandler()).end();
    
    newRouter.rule().async(false).msgType(XmlMsgType.VOICE)
    .handler(this.getVoiceHandler()).end();
    
    newRouter.rule().async(false).msgType(XmlMsgType.SHORTVIDEO)
    .handler(this.getMsgHandler()).end();
    
    newRouter.rule().async(false).msgType(XmlMsgType.VIDEO)
    .handler(this.getVideoHandler()).end();
    
    newRouter.rule().async(false).msgType(XmlMsgType.NEWS)
    .handler(this.getMsgHandler()).end();
    
    newRouter.rule().async(false).msgType(XmlMsgType.MUSIC)
    .handler(this.getMsgHandler()).end();
    
    
    newRouter.rule().async(false).msgType(XmlMsgType.DEVICE_TEXT)
    .handler(this.getMsgHandler()).end();
    
    newRouter.rule().async(false).msgType(XmlMsgType.DEVICE_EVENT)
    .handler(this.getMsgHandler()).end();
    
    newRouter.rule().async(false).msgType(XmlMsgType.DEVICE_STATUS)
    .handler(this.getMsgHandler()).end();
    
    newRouter.rule().async(false).msgType(XmlMsgType.HARDWARE)
    .handler(this.getMsgHandler()).end();
    
    newRouter.rule().async(false).msgType(XmlMsgType.TRANSFER_CUSTOMER_SERVICE)
    .handler(this.getMsgHandler()).end();
    
    // 默认
    newRouter.rule().async(false).handler(this.getMsgHandler()).end();

    return newRouter;
  }

  protected MpMenuHandler getMenuHandler() {
    return this.menuHandler;
  }

  protected MpSubscribeHandler getSubscribeHandler() {
    return this.subscribeHandler;
  }

  protected MpUnsubscribeHandler getUnsubscribeHandler() {
    return this.unsubscribeHandler;
  }

  protected MpAbstractHandler getLocationHandler() {
    return this.locationHandler;
  }

  protected MpMsgHandler getMsgHandler() {
    return this.msgHandler;
  }

//  protected MpAbstractHandler getScanHandler() {
//    return this.scanHandler;
//  }
  
  protected MpAbstractHandler getImageHandler() {
	    return this.imageHandler;
  }
  
  protected MpAbstractHandler getVideoHandler() {
	    return this.videoHandler;
  }
  
  protected MpAbstractHandler getVoiceHandler() {
	    return this.voiceHandler;
  }
  
 

}
