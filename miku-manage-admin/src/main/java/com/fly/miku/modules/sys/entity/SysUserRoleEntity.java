package com.fly.miku.modules.sys.entity;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户与角色对应关系
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年9月18日 上午9:28:39
 */
@TableName("sys_user_role")
@ApiModel(value="用户与角色对应关系")
@Data
public class SysUserRoleEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	@TableId
	@ApiModelProperty(value="ID")
	private Long id;

	/**
	 * 用户ID
	 */
	@ApiModelProperty(value="用户ID")
	private Long userId;

	/**
	 * 角色ID
	 */
	@ApiModelProperty(value="角色ID")
	private Long roleId;

	
}
