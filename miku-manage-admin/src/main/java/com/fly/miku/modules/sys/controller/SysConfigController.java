package com.fly.miku.modules.sys.controller;


import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fly.miku.common.annotation.SysLog;
import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.common.validator.ValidatorUtils;
import com.fly.miku.modules.sys.entity.SysConfigEntity;
import com.fly.miku.modules.sys.service.SysConfigService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 系统配置信息
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年12月4日 下午6:55:53
 */
@RestController
@RequestMapping("/sys/config")
@Api(tags  = "系统配置信息API")
public class SysConfigController extends AbstractController {
	@Autowired
	private SysConfigService sysConfigService;
	
	/**
	 * 所有配置列表
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@RequiresPermissions("sys:config:list")
    @ApiOperation("所有配置列表")
	public ResultWrapper<PageUtils> list(@RequestParam Map<String, Object> params){
		PageUtils page = sysConfigService.queryPage(params);
		return new ResultWrapper<PageUtils>(RespEnum.SUCCESS,page);
	}
	
	
	/**
	 * 配置信息
	 */
	@RequestMapping(value="/info/{id}",method=RequestMethod.GET)
	@RequiresPermissions("sys:config:info")
	@ApiOperation("配置信息")
	public ResultWrapper<SysConfigEntity> info(@PathVariable("id") Long id){
		SysConfigEntity config = sysConfigService.selectById(id);
		return new ResultWrapper<SysConfigEntity>(RespEnum.SUCCESS,config);
	}
	
	/**
	 * 保存配置
	 */
	@SysLog("保存配置")
	@RequestMapping(value="/save",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:config:save")
	@ApiOperation("保存配置")
	public ResultWrapper<Boolean> save(@RequestBody SysConfigEntity config){
		ValidatorUtils.validateEntity(config);

		sysConfigService.save(config);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 修改配置
	 */
	@SysLog("修改配置")
	@RequestMapping(value="/update",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:config:update")
	@ApiOperation("修改配置")
	public ResultWrapper<Boolean> update(@RequestBody SysConfigEntity config){
		ValidatorUtils.validateEntity(config);
		sysConfigService.update(config);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 删除配置
	 */
	@SysLog("删除配置")
	@RequestMapping(value="/delete",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:config:delete")
	@ApiOperation("删除配置")
	public ResultWrapper<Boolean> delete(@RequestBody Long[] ids){
		sysConfigService.deleteBatch(ids);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}

}
