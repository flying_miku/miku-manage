package com.fly.miku.modules.wechat.modules.mp.handler;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.fly.miku.modules.wechat.modules.mp.builder.MpImageBuilder;

import me.chanjar.weixin.common.api.WxConsts.XmlMsgType;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

@Component
public class MpImageHandler extends MpAbstractHandler {

	@Override
	public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
            Map<String, Object> context, WxMpService service,
            WxSessionManager sessionManager) {
		if (wxMessage.getMsgType().equals(XmlMsgType.IMAGE)) {
		      //TODO 自定义处理
	    	  return new MpImageBuilder().build(wxMessage.getMediaId(), wxMessage, service);
	    }
	    return null;
	}

}
