package com.fly.miku.modules.sys.entity;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 角色与菜单对应关系
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年9月18日 上午9:28:13
 */
@TableName("sys_role_menu")
@Data
@ApiModel("角色与菜单对应关系")
public class SysRoleMenuEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	@TableId
	@ApiModelProperty(value="ID")
	private Long id;

	/**
	 * 角色ID
	 */
	@ApiModelProperty(value="角色ID")
	private Long roleId;

	/**
	 * 菜单ID
	 */
	@ApiModelProperty(value="菜单ID")
	private Long menuId;

	
}
