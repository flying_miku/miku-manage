package com.fly.miku.modules.sys.entity;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableLogic;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 部门管理
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-06-20 15:23:47
 */
@TableName("sys_dept")
@Data
@ApiModel("部门管理")
public class SysDeptEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//部门ID
	@TableId
	@ApiModelProperty(value="部门ID")
	private Long deptId;
	
	//上级部门ID，一级部门为0
	@ApiModelProperty(value="上级部门ID，一级部门为0")
	private Long parentId;
	
	//部门名称
	@ApiModelProperty(value="部门名称")
	private String name;
	
	//上级部门名称
	@TableField(exist=false)
	@ApiModelProperty(value="上级部门名称")
	private String parentName;
	
	//排序
	@ApiModelProperty(value="排序")
	private Integer orderNum;

	@TableLogic
	@ApiModelProperty(value="删除标识")
	private Integer delFlag;
	/**
	 * ztree属性
	 */
	@TableField(exist=false)
	@ApiModelProperty(value="ztree属性 打开标识")
	private Boolean open;
	
	@TableField(exist=false)
	@ApiModelProperty(value="ztree属性 部门列表")
	private List<?> list;


}
