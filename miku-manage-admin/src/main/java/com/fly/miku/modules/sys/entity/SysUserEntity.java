package com.fly.miku.modules.sys.entity;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fly.miku.common.excel.annotation.ExcelField;
import com.fly.miku.common.excel.annotation.ExcelSheet;
import com.fly.miku.common.validator.group.AddGroup;
import com.fly.miku.common.validator.group.UpdateGroup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 系统用户
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年9月18日 上午9:28:55
 */
@TableName("sys_user")
@ApiModel(value="系统用户")
@Data
@ExcelSheet(name="系统用户")
public class SysUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 用户ID
	 */
	@TableId
	@ApiModelProperty(value="用户ID")
	@ExcelField(name = "用户ID")
	private Long userId;

	/**
	 * 用户名
	 */
	@NotBlank(message="用户名不能为空", groups = {AddGroup.class, UpdateGroup.class})
	@ApiModelProperty(value="用户名")
	@ExcelField(name = "用户名")
	private String username;

	/**
	 * 密码
	 */
	@NotBlank(message="密码不能为空", groups = AddGroup.class)
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@ApiModelProperty(value="密码")
	@ExcelField(name = "密码")
	private String password;

	/**
	 * 盐
	 */
	@ApiModelProperty(value="盐")
	@ExcelField(name = "盐")
	private String salt;

	/**
	 * 邮箱
	 */
	@NotBlank(message="邮箱不能为空", groups = {AddGroup.class, UpdateGroup.class})
	@Email(message="邮箱格式不正确", groups = {AddGroup.class, UpdateGroup.class})
	@ApiModelProperty(value="邮箱")
	@ExcelField(name = "邮箱")
	private String email;

	/**
	 * 手机号
	 */
	@ApiModelProperty(value="手机号")
	@ExcelField(name = "手机号")
	private String mobile;

	/**
	 * 状态  0：禁用   1：正常
	 */
	@ApiModelProperty(value="状态  0：禁用   1：正常")
	@ExcelField(name = "状态  0：禁用   1：正常")
	private Integer status;
	
	/**
	 * 角色ID列表
	 */
	@TableField(exist=false)
	@ApiModelProperty(value="角色ID列表")
	@ExcelField(name = "角色ID列表")
	private List<Long> roleIdList;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value="创建时间")
	@ExcelField(name = "创建时间")
	private Date createTime;

	/**
	 * 部门ID
	 */
	@NotNull(message="部门不能为空", groups = {AddGroup.class, UpdateGroup.class})
	@ApiModelProperty(value="部门ID")
	@ExcelField(name = "部门ID")
	private Long deptId;

	/**
	 * 部门名称
	 */
	@TableField(exist=false)
	@ApiModelProperty(value="部门名称")
	@ExcelField(name = "部门名称")
	private String deptName;

}
