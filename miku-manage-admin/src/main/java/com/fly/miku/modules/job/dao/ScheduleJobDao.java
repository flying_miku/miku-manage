package com.fly.miku.modules.job.dao;

import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fly.miku.modules.job.entity.ScheduleJobEntity;

/**
 * 定时任务
 *
 * @author Flying flying_miku@sina.com
 * @since 1.2.0 2016-11-28
 */
public interface ScheduleJobDao extends BaseMapper<ScheduleJobEntity> {
	
	/**
	 * 批量更新状态
	 */
	int updateBatch(Map<String, Object> map);
}
