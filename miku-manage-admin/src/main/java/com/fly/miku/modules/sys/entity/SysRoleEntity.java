package com.fly.miku.modules.sys.entity;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 角色
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年9月18日 上午9:27:38
 */
@TableName("sys_role")
@Data
@ApiModel("角色")
public class SysRoleEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 角色ID
	 */
	@TableId
	@ApiModelProperty(value="角色ID")
	private Long roleId;

	/**
	 * 角色名称
	 */
	@NotBlank(message="角色名称不能为空")
	@ApiModelProperty(value="角色名称")
	private String roleName;

	/**
	 * 备注
	 */
	@ApiModelProperty(value="备注")
	private String remark;

	/**
	 * 部门ID
	 */
	@NotNull(message="部门不能为空")
	@ApiModelProperty(value="部门ID")
	private Long deptId;

	/**
	 * 部门名称
	 */
	@TableField(exist=false)
	@ApiModelProperty(value="部门名称")
	private String deptName;

	@TableField(exist=false)
	@ApiModelProperty(value="菜单列表")
	private List<Long> menuIdList;
	
	@TableField(exist=false)
	@ApiModelProperty(value="部门列表")
	private List<Long> deptIdList;
	
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value="创建时间")
	private Date createTime;

}
