package com.fly.miku.modules.job.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.modules.job.entity.ScheduleJobLogEntity;

/**
 * 定时任务日志
 *
 * @author Flying flying_miku@sina.com
 * @since 1.2.0 2016-11-28
 */
public interface ScheduleJobLogService extends IService<ScheduleJobLogEntity> {

	PageUtils queryPage(Map<String, Object> params);
	
}
