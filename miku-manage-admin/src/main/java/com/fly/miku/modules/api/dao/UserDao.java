package com.fly.miku.modules.api.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fly.miku.modules.api.entity.UserEntity;

/**
 * 用户
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-03-23 15:22:06
 */
public interface UserDao extends BaseMapper<UserEntity> {

}
