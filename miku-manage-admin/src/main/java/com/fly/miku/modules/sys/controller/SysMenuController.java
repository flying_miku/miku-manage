package com.fly.miku.modules.sys.controller;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fly.miku.common.annotation.SysLog;
import com.fly.miku.common.exception.FLYException;
import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.Constant;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.modules.sys.entity.SysMenuEntity;
import com.fly.miku.modules.sys.service.SysMenuService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 系统菜单
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年10月27日 下午9:58:15
 */
@RestController
@RequestMapping(value="/sys/menu")
@Api(tags = "系统菜单API")
public class SysMenuController extends AbstractController {
	@Autowired
	private SysMenuService sysMenuService;

	/**
	 * 导航菜单
	 */
	@RequestMapping(value="/nav",method=RequestMethod.GET)
	@ApiOperation("导航菜单")
	public ResultWrapper<List<SysMenuEntity>>  nav(){
		List<SysMenuEntity> menuList = sysMenuService.getUserMenuList(getUserId());
		return new ResultWrapper<List<SysMenuEntity>>(RespEnum.SUCCESS,menuList);
	}
	
	/**
	 * 所有菜单列表
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@RequiresPermissions("sys:menu:list")
	@ApiOperation("所有菜单列表")
	public List<SysMenuEntity> list(){
		List<SysMenuEntity> menuList = sysMenuService.selectList(null);
		for(SysMenuEntity sysMenuEntity : menuList){
			SysMenuEntity parentMenuEntity = sysMenuService.selectById(sysMenuEntity.getParentId());
			if(parentMenuEntity != null){
				sysMenuEntity.setParentName(parentMenuEntity.getName());
			}
		}

		return menuList;
	}
	
	/**
	 * 选择菜单(添加、修改菜单)
	 */
	@RequestMapping(value="/select",method=RequestMethod.GET)
	@RequiresPermissions("sys:menu:select")
	@ApiOperation("选择菜单(添加、修改菜单)")
	public ResultWrapper<List<SysMenuEntity>> select(){
		//查询列表数据
		List<SysMenuEntity> menuList = sysMenuService.queryNotButtonList();
		
		//添加顶级菜单
		SysMenuEntity root = new SysMenuEntity();
		root.setMenuId(0L);
		root.setName("一级菜单");
		root.setParentId(-1L);
		root.setOpen(true);
		menuList.add(root);
		return new ResultWrapper<List<SysMenuEntity>>(RespEnum.SUCCESS,menuList);
	}
	
	/**
	 * 菜单信息
	 */
	@RequestMapping(value="/info/{menuId}",method=RequestMethod.GET)
	@RequiresPermissions("sys:menu:info")
	@ApiOperation("菜单信息")
	public ResultWrapper<SysMenuEntity> info(@PathVariable("menuId") Long menuId){
		SysMenuEntity menu = sysMenuService.selectById(menuId);
		return new ResultWrapper<SysMenuEntity>(RespEnum.SUCCESS,menu);
	}
	
	/**
	 * 保存
	 */
	@SysLog("保存菜单")
	@RequestMapping(value="/save",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:menu:save")
	@ApiOperation("保存菜单")
	public ResultWrapper<Boolean> save(@RequestBody SysMenuEntity menu){
		//数据校验
		verifyForm(menu);
		
		sysMenuService.insert(menu);
		
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 修改
	 */
	@SysLog("修改菜单")
	@RequestMapping(value="/update",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:menu:update")
	@ApiOperation("修改")
	public ResultWrapper<Boolean> update(@RequestBody SysMenuEntity menu){
		//数据校验
		verifyForm(menu);
				
		sysMenuService.updateById(menu);
		
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 删除
	 */
	@SysLog("删除菜单")
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	@RequiresPermissions("sys:menu:delete")
	@ApiOperation("删除")
	public ResultWrapper<Boolean> delete(long menuId){
		if(menuId <= 31){
			return new ResultWrapper<Boolean>(RespEnum.SYSTEM_MENU_CAN_NOT_BE_DELETED);
		}

		//判断是否有子菜单或按钮
		List<SysMenuEntity> menuList = sysMenuService.queryListParentId(menuId);
		if(menuList.size() > 0){
			return new ResultWrapper<Boolean>(RespEnum.PLEASE_DELETE_THE_SUBMENU_OR_BUTTON_FIRST);
		}

		sysMenuService.delete(menuId);

		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 验证参数是否正确
	 */
	private void verifyForm(SysMenuEntity menu){
		if(StringUtils.isBlank(menu.getName())){
			throw new FLYException(RespEnum.MENU_NAME_NOT_NULL);
		}
		
		if(menu.getParentId() == null){
			throw new FLYException(RespEnum.SUPERIOR_MENUS_CANNOT_BE_EMPTY);
		}
		
		//菜单
		if(menu.getType() == Constant.MenuType.MENU.getValue()){
			if(StringUtils.isBlank(menu.getUrl())){
				throw new FLYException(RespEnum.SUPERIOR_MENU_URL_NOT_NULL);
			}
		}
		
		//上级菜单类型
		int parentType = Constant.MenuType.CATALOG.getValue();
		if(menu.getParentId() != 0){
			SysMenuEntity parentMenu = sysMenuService.selectById(menu.getParentId());
			parentType = parentMenu.getType();
		}
		
		//目录、菜单
		if(menu.getType() == Constant.MenuType.CATALOG.getValue() ||
				menu.getType() == Constant.MenuType.MENU.getValue()){
			if(parentType != Constant.MenuType.CATALOG.getValue()){
				throw new FLYException(RespEnum.SUPERIOR_MENU_IS_ONLY_A_DIRECTORY_TYPE);
			}
			return ;
		}
		
		//按钮
		if(menu.getType() == Constant.MenuType.BUTTON.getValue()){
			if(parentType != Constant.MenuType.MENU.getValue()){
				throw new FLYException(RespEnum.THE_SUPERIOR_MENU_IS_ONLY_FOR_THE_MENU_TYPE);
			}
			return ;
		}
	}
}
