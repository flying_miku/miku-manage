package com.fly.miku.modules.oss.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.modules.oss.entity.SysOssEntity;

/**
 * 文件上传
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-03-25 12:13:26
 */
public interface SysOssService extends IService<SysOssEntity> {

	PageUtils queryPage(Map<String, Object> params);
}
