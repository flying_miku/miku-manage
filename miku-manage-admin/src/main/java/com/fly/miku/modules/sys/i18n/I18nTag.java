package com.fly.miku.modules.sys.i18n;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

/**
 * Shiro权限标签
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年12月3日 下午11:32:47
 */
@Component
public class I18nTag {

	
	/**
	 * 加载js  语言
	 * @return   true：是     false：否
	 */
	public String loadlLaguage() {
		return LocaleContextHolder.getLocale().toString();
	}
	
	

}
