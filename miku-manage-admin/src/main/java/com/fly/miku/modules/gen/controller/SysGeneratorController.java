package com.fly.miku.modules.gen.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.modules.gen.service.SysGeneratorService;
import com.fly.miku.modules.gen.utils.PageUtils;
import com.fly.miku.modules.gen.utils.Query;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 代码生成器
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年12月19日 下午9:12:58
 */
@Controller
@RequestMapping(value="/sys/generator")
@Api(tags ="代码生成器API")
public class SysGeneratorController {
	@Autowired
	private SysGeneratorService sysGeneratorService;
	
	/**
	 * 列表
	 */
	@ResponseBody
	@RequestMapping(value="/list",method=RequestMethod.GET)
    @ApiOperation("列表")
	public ResultWrapper<PageUtils> list(@RequestParam Map<String, Object> params){
		//查询列表数据
		Query query = new Query(params);
		List<Map<String, Object>> list = sysGeneratorService.queryList(query);
		int total = sysGeneratorService.queryTotal(query);
		
		PageUtils pageUtil = new PageUtils(list, total, query.getLimit(), query.getPage());
		
		return new ResultWrapper<PageUtils>(RespEnum.SUCCESS,pageUtil);

	}
	
	/**
	 * 生成代码
	 */
	@RequestMapping(value="/code",method=RequestMethod.GET)
	@ApiOperation("生成代码")
	public void code(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String[] tableNames = new String[]{};
		String tables = request.getParameter("tables");
		tableNames = JSON.parseArray(tables).toArray(tableNames);
		
		byte[] data = sysGeneratorService.generatorCode(tableNames);
		
		response.reset();  
        response.setHeader("Content-Disposition", "attachment; filename=\"miku.zip\"");  
        response.addHeader("Content-Length", "" + data.length);  
        response.setContentType("application/octet-stream; charset=UTF-8");  
  
        IOUtils.write(data, response.getOutputStream());  
	}
}
