package com.fly.miku.modules.sys.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 系统页面视图
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年11月24日 下午11:05:27
 */
@Controller
public class SysPageController {
	
	@RequestMapping(value="modules/{module}/{url}.html",method=RequestMethod.GET)
	public String module(@PathVariable("module") String module, @PathVariable("url") String url){
		return "modules/" + module + "/" + url;
	}

	@RequestMapping(value = {"/", "index.html"},method=RequestMethod.GET)
	public String index(){
		return "index";
	}

	@RequestMapping(value="index1.html",method=RequestMethod.GET)
	public String index1(){
		return "index1";
	}

	@RequestMapping(value="login.html",method=RequestMethod.GET)
	public String login(){
		return "login";
	}

	@RequestMapping(value="main.html",method=RequestMethod.GET)
	public String main(){
		return "main";
	}

	@RequestMapping(value="404.html",method=RequestMethod.GET)
	public String notFound(){
		return "404";
	}

}
