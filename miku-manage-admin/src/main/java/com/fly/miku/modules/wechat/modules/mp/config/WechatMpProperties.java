package com.fly.miku.modules.wechat.modules.mp.config;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * wechat mp properties
 *
 * @author Binary Wang(https://github.com/binarywang)
 */
@ConfigurationProperties(prefix = "wechat.mp")
@Data
public class WechatMpProperties   {

	/**
	 * 设置公众号的appid
	 */
	private String appId;

	/**
	 * 设置公众号的Secret
	 */
	private String secret;

	/**
	 * 设置公众号的token
	 */
	private String token;

	/**
	 * 设置公众号的EncodingAESKey
	 */
	private String aesKey;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
