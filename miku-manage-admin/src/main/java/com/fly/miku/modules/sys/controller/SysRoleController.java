package com.fly.miku.modules.sys.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fly.miku.common.annotation.SysLog;
import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.common.validator.ValidatorUtils;
import com.fly.miku.modules.sys.entity.SysRoleEntity;
import com.fly.miku.modules.sys.service.SysRoleDeptService;
import com.fly.miku.modules.sys.service.SysRoleMenuService;
import com.fly.miku.modules.sys.service.SysRoleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 角色管理
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年11月8日 下午2:18:33
 */
@RestController
@RequestMapping(value="/sys/role")
@Api(tags = "角色管理API")
public class SysRoleController extends AbstractController {
	@Autowired
	private SysRoleService sysRoleService;
	@Autowired
	private SysRoleMenuService sysRoleMenuService;
	@Autowired
	private SysRoleDeptService sysRoleDeptService;
	
	/**
	 * 角色列表
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@RequiresPermissions("sys:role:list")
	@ApiOperation("角色列表")
	public ResultWrapper<PageUtils> list(@RequestParam Map<String, Object> params){
		PageUtils page = sysRoleService.queryPage(params);
		return new ResultWrapper<PageUtils>(RespEnum.SUCCESS, page);
	}
	
	/**
	 * 角色列表
	 */
	@RequestMapping(value="/select",method=RequestMethod.GET)
	@RequiresPermissions("sys:role:select")
	@ApiOperation("角色列表")
	public ResultWrapper<List<SysRoleEntity>> select(){
		List<SysRoleEntity> list = sysRoleService.selectList(null);
		return new ResultWrapper<List<SysRoleEntity>>(RespEnum.SUCCESS, list);
	}
	
	/**
	 * 角色信息
	 */
	@RequestMapping(value="/info/{roleId}",method=RequestMethod.GET)
	@RequiresPermissions("sys:role:info")
	@ApiOperation("角色信息")
	public ResultWrapper<SysRoleEntity> info(@PathVariable("roleId") Long roleId){
		SysRoleEntity role = sysRoleService.selectById(roleId);
		
		//查询角色对应的菜单
		List<Long> menuIdList = sysRoleMenuService.queryMenuIdList(roleId);
		role.setMenuIdList(menuIdList);

		//查询角色对应的部门
		List<Long> deptIdList = sysRoleDeptService.queryDeptIdList(new Long[]{roleId});
		role.setDeptIdList(deptIdList);
		
		return new ResultWrapper<SysRoleEntity>(RespEnum.SUCCESS, role);
	}
	
	/**
	 * 保存角色
	 */
	@SysLog("保存角色")
	@RequestMapping(value="/save",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:role:save")
	@ApiOperation("保存角色")
	public ResultWrapper<Boolean> save(@RequestBody SysRoleEntity role){
		ValidatorUtils.validateEntity(role);
		sysRoleService.save(role);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 修改角色
	 */
	@SysLog("修改角色")
	@RequestMapping(value="/update",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:role:update")
	@ApiOperation("修改角色")
	public ResultWrapper<Boolean> update(@RequestBody SysRoleEntity role){
		ValidatorUtils.validateEntity(role);
		
		sysRoleService.update(role);
		
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 删除角色
	 */
	@SysLog("删除角色")
	@RequestMapping(value="/delete",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:role:delete")
	@ApiOperation("删除角色")
	public ResultWrapper<Boolean> delete(@RequestBody Long[] roleIds){
		sysRoleService.deleteBatch(roleIds);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
}
