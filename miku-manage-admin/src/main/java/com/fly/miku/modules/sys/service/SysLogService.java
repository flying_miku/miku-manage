package com.fly.miku.modules.sys.service;


import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.modules.sys.entity.SysLogEntity;


/**
 * 系统日志
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-03-08 10:40:56
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

}
