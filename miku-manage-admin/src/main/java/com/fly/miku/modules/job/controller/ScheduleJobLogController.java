package com.fly.miku.modules.job.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.modules.job.entity.ScheduleJobLogEntity;
import com.fly.miku.modules.job.service.ScheduleJobLogService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 定时任务日志
 *
 * @author Flying flying_miku@sina.com
 * @since 1.2.0 2016-11-28
 */
@RestController
@RequestMapping(value="/sys/scheduleLog")
@Api(tags = "定时任务日志")
public class ScheduleJobLogController {
	@Autowired
	private ScheduleJobLogService scheduleJobLogService;
	
	/**
	 * 定时任务日志列表
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@RequiresPermissions("sys:schedule:log")
	@ApiOperation("定时任务日志列表")
	public ResultWrapper<PageUtils> list(@RequestParam Map<String, Object> params){
		PageUtils page = scheduleJobLogService.queryPage(params);
		return new ResultWrapper<PageUtils>(RespEnum.SUCCESS,page);
	}
	
	/**
	 * 定时任务日志信息
	 */
	@RequestMapping(value="/info/{logId}",method=RequestMethod.GET)
	@ApiOperation("定时任务日志信息")
	public ResultWrapper<ScheduleJobLogEntity> info(@PathVariable("logId") Long logId){
		ScheduleJobLogEntity log = scheduleJobLogService.selectById(logId);
		return new ResultWrapper<ScheduleJobLogEntity>(RespEnum.SUCCESS,log);
	}
}
