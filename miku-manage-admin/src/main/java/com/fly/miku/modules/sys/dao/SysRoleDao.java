package com.fly.miku.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fly.miku.modules.sys.entity.SysRoleEntity;

/**
 * 角色管理
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年9月18日 上午9:33:33
 */
public interface SysRoleDao extends BaseMapper<SysRoleEntity> {
	

}
