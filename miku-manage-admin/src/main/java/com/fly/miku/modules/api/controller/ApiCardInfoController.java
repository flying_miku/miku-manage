package com.fly.miku.modules.api.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baidu.aip.ocr.AipOcr;
import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.modules.api.entity.CardInfoEntity;
import com.fly.miku.modules.api.service.CardInfoService;



/**
 * 
 *
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2018-02-22 17:55:00
 */
@RestController
@RequestMapping("api/cardinfo")
public class ApiCardInfoController {
    @Autowired
    private CardInfoService cardInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("api:cardinfo:list")
    public ResultWrapper<PageUtils> list(@RequestParam Map<String, Object> params){
        PageUtils page = cardInfoService.queryPage(params);
        return new ResultWrapper<PageUtils>(RespEnum.SUCCESS, page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("api:cardinfo:info")
    public ResultWrapper<CardInfoEntity> info(@PathVariable("id") Integer id){
		CardInfoEntity cardInfo = cardInfoService.selectById(id);
        return new ResultWrapper<CardInfoEntity>(RespEnum.SUCCESS, cardInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("api:cardinfo:save")
    public ResultWrapper<Boolean> save(@RequestBody CardInfoEntity cardInfo){
    	
		cardInfo.setCreaDate(new Date());
		cardInfo.setContent(getContent(cardInfo));
		cardInfoService.insert(cardInfo);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);

    }
    
    /**
     * 获取识别结果集
     * @param cardInfo
     * @return
     */
    private String getContent(CardInfoEntity cardInfo){
    	StringBuffer sb = new StringBuffer();
        JSONObject res   = baiOcr(cardInfo.getPositiveImage());
        Integer num  = res.getInt("words_result_num");
        if(num>0){
        	JSONArray jsonArray = res.getJSONArray("words_result");
        	for(int i=0;i<num;i++){
        		sb.append(jsonArray.getJSONObject(i).getString("words"));
        	}
        }
        res = baiOcr(cardInfo.getNegativeImage());
        num  = res.getInt("words_result_num");
        if(num>0){
        	JSONArray jsonArray = res.getJSONArray("words_result");
        	for(int i=0;i<num;i++){
        		sb.append(jsonArray.getJSONObject(i).getString("words"));
        	}
        }
        return sb.toString();
    }
    
    /**
     * 百度识别
     * @param path
     * @return
     */
    private JSONObject baiOcr(String path){
    	 String APP_ID = "10843212";
	    String API_KEY = "t9f5DazyuK7KXuUAmaVdbQsq";
	    String SECRET_KEY = "rgDECf5QBntyFGVsD5Dyfuh0qZCBnRoU";
	    // 初始化一个AipOcr
	    AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);
        return client.basicGeneralUrl(path, new HashMap<String, String>());
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("api:cardinfo:update")
    public ResultWrapper<Boolean> update(@RequestBody CardInfoEntity cardInfo){
    		cardInfo.setContent(getContent(cardInfo));
			cardInfoService.updateById(cardInfo);
			return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("api:cardinfo:delete")
    public ResultWrapper<Boolean> delete(@RequestBody Integer[] ids){
			cardInfoService.deleteBatchIds(Arrays.asList(ids));
			return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
    }
    
    /**
     * 备份数据
     * @throws IOException 
     */
    @RequestMapping("/databaseBackup")
    @RequiresPermissions("api:cardinfo:databaseBackup")
    public ResultWrapper<Map<String,String>> databaseBackup(String path) throws IOException{
    	File file = new File(path);
    	if((file.exists()&&file.isFile())){
    		return new ResultWrapper<Map<String,String>>(RespEnum.PLEASE_SELECT_THE_DIRECTORY);
    	}
    	if(!file.exists()){
    		file.mkdirs();
    	}
    	String backupFileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+".sql";
    	
    	List<CardInfoEntity> list = cardInfoService.selectList(null);
    	StringBuffer sb = new StringBuffer();
    	for (CardInfoEntity cardInfoEntity : list) {
    		sb.append("insert into card_info (id, content, positive_image, negative_image, crea_date) values ('");
    		sb.append(cardInfoEntity.getId());
    		sb.append("', '");
    		sb.append(cardInfoEntity.getContent());
    		sb.append("', '");
    		sb.append(cardInfoEntity.getPositiveImage());
    		sb.append("', '");
    		sb.append(cardInfoEntity.getNegativeImage());
    		sb.append("', '");
    		sb.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cardInfoEntity.getCreaDate()));
    		sb.append("');");
    		sb.append("\r\n");
		}
    	File newFile = new File(file,backupFileName);
    	newFile.createNewFile();
    	 FileWriter fw = new FileWriter(newFile);
         BufferedWriter bw = new BufferedWriter(fw);
         bw.write(sb.toString());
         bw.close();
         fw.close();
		return new ResultWrapper<Map<String,String>>(RespEnum.SUCCESS,"备份成功");
    }

}
