package com.fly.miku.modules.job.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fly.miku.modules.job.entity.ScheduleJobLogEntity;

/**
 * 定时任务日志
 *
 * @author Flying flying_miku@sina.com
 * @since 1.2.0 2016-11-28
 */
public interface ScheduleJobLogDao extends BaseMapper<ScheduleJobLogEntity> {
	
}
