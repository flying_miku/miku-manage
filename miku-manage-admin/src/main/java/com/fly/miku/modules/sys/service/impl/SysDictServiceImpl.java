package com.fly.miku.modules.sys.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.Query;
import com.fly.miku.modules.sys.dao.SysDictDao;
import com.fly.miku.modules.sys.entity.SysDictEntity;
import com.fly.miku.modules.sys.service.SysDictService;


@Service("sysDictService")
public class SysDictServiceImpl extends ServiceImpl<SysDictDao, SysDictEntity> implements SysDictService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = (String)params.get("name");
        Wrapper<SysDictEntity>  wrapper = new EntityWrapper<SysDictEntity>()
        .like(StringUtils.isNotBlank(name),"name", name);
        Page<SysDictEntity> page = this.selectPage(
                new Query<SysDictEntity>(params).getPage(),
                wrapper
        );
        page.setTotal(this.selectCount(wrapper));
        return new PageUtils(page);
    }

}
