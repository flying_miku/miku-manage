package com.fly.miku.modules.sys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fly.miku.modules.sys.entity.SysLogEntity;

/**
 * 系统日志
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-03-08 10:40:56
 */
public interface SysLogDao extends BaseMapper<SysLogEntity> {
	
}
