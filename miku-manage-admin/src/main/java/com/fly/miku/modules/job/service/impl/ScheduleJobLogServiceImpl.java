package com.fly.miku.modules.job.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.Query;
import com.fly.miku.modules.job.dao.ScheduleJobLogDao;
import com.fly.miku.modules.job.entity.ScheduleJobLogEntity;
import com.fly.miku.modules.job.service.ScheduleJobLogService;

@Service("scheduleJobLogService")
public class ScheduleJobLogServiceImpl extends ServiceImpl<ScheduleJobLogDao, ScheduleJobLogEntity> implements ScheduleJobLogService {

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String jobId = (String)params.get("jobId");
		Wrapper<ScheduleJobLogEntity> wrapper = new EntityWrapper<ScheduleJobLogEntity>().like(StringUtils.isNotBlank(jobId),"job_id", jobId);
		Page<ScheduleJobLogEntity> page = this.selectPage(
				new Query<ScheduleJobLogEntity>(params).getPage(),
				wrapper
		);
		page.setTotal(this.selectCount(wrapper));
		return new PageUtils(page);
	}

}
