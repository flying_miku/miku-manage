package com.fly.miku.modules.job.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fly.miku.common.annotation.SysLog;
import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.common.validator.ValidatorUtils;
import com.fly.miku.modules.job.entity.ScheduleJobEntity;
import com.fly.miku.modules.job.service.ScheduleJobService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 定时任务
 *
 * @author Flying flying_miku@sina.com
 * @since 1.2.0 2016-11-28
 */
@RestController
@RequestMapping(value="/sys/schedule")
@Api(tags = "定时任务")
public class ScheduleJobController {
	@Autowired
	private ScheduleJobService scheduleJobService;
	
	/**
	 * 定时任务列表
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@RequiresPermissions("sys:schedule:list")
	@ApiOperation("定时任务列表")
	public ResultWrapper<PageUtils> list(@RequestParam Map<String, Object> params){
		PageUtils page = scheduleJobService.queryPage(params);
		return new ResultWrapper<PageUtils>(RespEnum.SUCCESS,page);
	}
	
	/**
	 * 定时任务信息
	 */
	@RequestMapping(value="/info/{jobId}",method=RequestMethod.GET)
	@RequiresPermissions("sys:schedule:info")
	@ApiOperation("定时任务信息")
	public ResultWrapper<ScheduleJobEntity> info(@PathVariable("jobId") Long jobId){
		ScheduleJobEntity schedule = scheduleJobService.selectById(jobId);
		return new ResultWrapper<ScheduleJobEntity>(RespEnum.SUCCESS,schedule);
	}
	
	/**
	 * 保存定时任务
	 */
	@SysLog("保存定时任务")
	@RequestMapping(value="/save",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:schedule:save")
	@ApiOperation("保存定时任务")
	public ResultWrapper<Boolean> save(@RequestBody ScheduleJobEntity scheduleJob){
		ValidatorUtils.validateEntity(scheduleJob);
		scheduleJobService.save(scheduleJob);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 修改定时任务
	 */
	@SysLog("修改定时任务")
	@RequestMapping(value="/update",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:schedule:update")
	@ApiOperation("修改定时任务")
	public ResultWrapper<Boolean> update(@RequestBody ScheduleJobEntity scheduleJob){
		ValidatorUtils.validateEntity(scheduleJob);
				
		scheduleJobService.update(scheduleJob);
		
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 删除定时任务
	 */
	@SysLog("删除定时任务")
	@RequestMapping(value="/delete",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:schedule:delete")
	@ApiOperation("删除定时任务")
	public ResultWrapper<Boolean> delete(@RequestBody Long[] jobIds){
		scheduleJobService.deleteBatch(jobIds);
		
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 立即执行任务
	 */
	@SysLog("立即执行任务")
	@RequestMapping(value="/run",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:schedule:run")
	@ApiOperation("立即执行任务")
	public ResultWrapper<Boolean> run(@RequestBody Long[] jobIds){
		scheduleJobService.run(jobIds);
		
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 暂停定时任务
	 */
	@SysLog("暂停定时任务")
	@RequestMapping(value="/pause",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:schedule:pause")
	@ApiOperation("暂停定时任务")
	public ResultWrapper<Boolean> pause(@RequestBody Long[] jobIds){
		scheduleJobService.pause(jobIds);
		
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 恢复定时任务
	 */
	@SysLog("恢复定时任务")
	@RequestMapping(value="/resume",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:schedule:resume")
	@ApiOperation("恢复定时任务")
	public ResultWrapper<Boolean> resume(@RequestBody Long[] jobIds){
		scheduleJobService.resume(jobIds);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}

}
