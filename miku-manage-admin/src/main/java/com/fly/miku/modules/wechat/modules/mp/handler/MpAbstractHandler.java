package com.fly.miku.modules.wechat.modules.mp.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import me.chanjar.weixin.mp.api.WxMpMessageHandler;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
public abstract class MpAbstractHandler implements WxMpMessageHandler {
  protected Logger logger = LoggerFactory.getLogger(getClass());
}
