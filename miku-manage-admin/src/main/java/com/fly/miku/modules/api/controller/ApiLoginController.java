package com.fly.miku.modules.api.controller;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.common.validator.ValidatorUtils;
import com.fly.miku.modules.api.annotation.Login;
import com.fly.miku.modules.api.form.LoginForm;
import com.fly.miku.modules.api.service.TokenService;
import com.fly.miku.modules.api.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 登录接口
 *
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-03-23 15:31
 */
@RestController
@RequestMapping("/api")
@Api(tags="登录接口")
public class ApiLoginController {
    @Autowired
    private UserService userService;
    @Autowired
    private TokenService tokenService;


    @PostMapping("login")
    @ApiOperation("登录")
    public ResultWrapper<Map<String, Object>> login(@RequestBody LoginForm form){
        //表单校验
        ValidatorUtils.validateEntity(form);

        //用户登录
        Map<String, Object> map = userService.login(form);
        return new ResultWrapper<Map<String, Object>>(RespEnum.SUCCESS,map);

    }

    @Login
    @PostMapping("logout")
    @ApiOperation("退出")
    public ResultWrapper<Boolean> logout(@ApiIgnore @RequestAttribute("userId") long userId){
        tokenService.expireToken(userId);
        return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
    }

}
