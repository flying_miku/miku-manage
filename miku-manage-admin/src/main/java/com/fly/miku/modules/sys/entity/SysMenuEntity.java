package com.fly.miku.modules.sys.entity;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 菜单管理
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年9月18日 上午9:26:39
 */
@TableName("sys_menu")
@Data
@ApiModel("菜单管理")
public class SysMenuEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 菜单ID
	 */
	@TableId
	@ApiModelProperty(value="菜单ID")
	private Long menuId;

	/**
	 * 父菜单ID，一级菜单为0
	 */
	@ApiModelProperty(value="父菜单ID，一级菜单为0")
	private Long parentId;
	
	/**
	 * 父菜单名称
	 */
	@TableField(exist=false)
	@ApiModelProperty(value="父菜单名称")
	private String parentName;

	/**
	 * 菜单名称
	 */
	@ApiModelProperty(value="菜单名称")
	private String name;
	
	/**
	 * 菜单名称
	 */
	@ApiModelProperty(value="菜单英文名称")
	private String englishName;
	
	/**
	 * 报表名
	 */
	@ApiModelProperty(value="报表名")
	private String table;
	
	
	/**
	 * 报表名称
	 */
	@TableField(exist=false)
	@ApiModelProperty(value="报表名称")
	private String tableName;
	

	/**
	 * 菜单URL
	 */
	@ApiModelProperty(value="菜单URL")
	private String url;

	/**
	 * 授权(多个用逗号分隔，如：user:list,user:create)
	 */
	@ApiModelProperty(value="授权(多个用逗号分隔，如：user:list,user:create)")
	private String perms;

	/**
	 * 类型     0：目录   1：菜单   2：按钮
	 */
	@ApiModelProperty(value="类型     0：目录   1：菜单   2：按钮")
	private Integer type;

	/**
	 * 菜单图标
	 */
	@ApiModelProperty(value="菜单图标")
	private String icon;

	/**
	 * 排序
	 */
	@ApiModelProperty(value="排序")
	private Integer orderNum;
	
	/**
	 * ztree属性
	 */
	@TableField(exist=false)
	@ApiModelProperty(value="ztree属性 打开标识")
	private Boolean open;

	
	@TableField(exist=false)
	@ApiModelProperty(value="ztree属性 菜单列表")
	private List<?> list;

}
