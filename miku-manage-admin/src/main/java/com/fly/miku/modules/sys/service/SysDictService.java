package com.fly.miku.modules.sys.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.modules.sys.entity.SysDictEntity;

/**
 * 数据字典
 *
 * @author Flying flying_miku@sina.com
 * @since 3.1.0 2018-01-27
 */
public interface SysDictService extends IService<SysDictEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

