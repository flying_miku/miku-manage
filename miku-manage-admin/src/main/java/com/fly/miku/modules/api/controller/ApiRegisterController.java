package com.fly.miku.modules.api.controller;


import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.common.validator.ValidatorUtils;
import com.fly.miku.modules.api.entity.UserEntity;
import com.fly.miku.modules.api.form.RegisterForm;
import com.fly.miku.modules.api.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 注册接口
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-03-26 17:27
 */
@RestController
@RequestMapping("/api")
@Api(tags="注册接口")
public class ApiRegisterController {
    @Autowired
    private UserService userService;

    @PostMapping("register")
    @ApiOperation("注册")
    public ResultWrapper<Boolean> register(@RequestBody RegisterForm form){
        //表单校验
        ValidatorUtils.validateEntity(form);

        UserEntity user = new UserEntity();
        user.setMobile(form.getMobile());
        user.setUsername(form.getMobile());
        user.setPassword(DigestUtils.sha256Hex(form.getPassword()));
        user.setCreateTime(new Date());
        userService.insert(user);

        return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
    }
}
