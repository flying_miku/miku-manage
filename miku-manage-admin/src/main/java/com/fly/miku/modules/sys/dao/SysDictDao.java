package com.fly.miku.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fly.miku.modules.sys.entity.SysDictEntity;

/**
 * 数据字典
 *
 * @author Flying flying_miku@sina.com
 * @since 3.1.0 2018-01-27
 */
public interface SysDictDao extends BaseMapper<SysDictEntity> {
	
}
