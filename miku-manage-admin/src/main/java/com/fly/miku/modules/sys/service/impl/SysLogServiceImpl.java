package com.fly.miku.modules.sys.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.Query;
import com.fly.miku.modules.sys.dao.SysLogDao;
import com.fly.miku.modules.sys.entity.SysLogEntity;
import com.fly.miku.modules.sys.service.SysLogService;


@Service("sysLogService")
public class SysLogServiceImpl extends ServiceImpl<SysLogDao, SysLogEntity> implements SysLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String)params.get("key");
        Wrapper<SysLogEntity> wrapper =new EntityWrapper<SysLogEntity>().like(StringUtils.isNotBlank(key),"username", key);
        Page<SysLogEntity> page = this.selectPage(
            new Query<SysLogEntity>(params).getPage(),
            wrapper
        );
        page.setTotal(this.selectCount(wrapper));
        return new PageUtils(page);
    }
}
