package com.fly.miku.modules.oss.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.Query;
import com.fly.miku.modules.oss.dao.SysOssDao;
import com.fly.miku.modules.oss.entity.SysOssEntity;
import com.fly.miku.modules.oss.service.SysOssService;


@Service("sysOssService")
public class SysOssServiceImpl extends ServiceImpl<SysOssDao, SysOssEntity> implements SysOssService {

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		Wrapper<SysOssEntity> wrapper = new EntityWrapper<SysOssEntity>();
		Page<SysOssEntity> page = this.selectPage(
				new Query<SysOssEntity>(params).getPage()
		);
        page.setTotal(this.selectCount(wrapper));
		return new PageUtils(page);
	}
	
}
