package com.fly.miku.modules.sys.controller;

import java.util.HashMap;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.Constant;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.modules.sys.entity.SysDeptEntity;
import com.fly.miku.modules.sys.service.SysDeptService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * 部门管理
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-06-20 15:23:47
 */
@RestController
@RequestMapping("/sys/dept")
@Api(tags = "部门管理API")
public class SysDeptController extends AbstractController {
	@Autowired
	private SysDeptService sysDeptService;
	
	/**
	 * 列表
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@RequiresPermissions("sys:dept:list")
	@ApiOperation("列表")
	public List<SysDeptEntity> list(){
		List<SysDeptEntity> deptList = sysDeptService.queryList(new HashMap<String, Object>());
		return deptList;
	}

	/**
	 * 选择部门(添加、修改菜单)
	 */
	@RequestMapping(value="/select",method=RequestMethod.GET)
	@RequiresPermissions("sys:dept:select")
	@ApiOperation("选择部门(添加、修改菜单)")
	public ResultWrapper<List<SysDeptEntity>> select(){
		List<SysDeptEntity> deptList = sysDeptService.queryList(new HashMap<String, Object>());

		//添加一级部门
		if(getUserId() == Constant.SUPER_ADMIN){
			SysDeptEntity root = new SysDeptEntity();
			root.setDeptId(0L);
			root.setName("一级部门");
			root.setParentId(-1L);
			root.setOpen(true);
			deptList.add(root);
		}
		return new ResultWrapper<List<SysDeptEntity>>(RespEnum.SUCCESS,deptList);
	}

	/**
	 * 上级部门Id(管理员则为0)
	 */
	@RequestMapping(value="/info",method=RequestMethod.GET)
	@RequiresPermissions("sys:dept:list")
	@ApiOperation("上级部门Id(管理员则为0)")
	public ResultWrapper<Long> info(){
		long deptId = 0;
		if(getUserId() != Constant.SUPER_ADMIN){
			List<SysDeptEntity> deptList = sysDeptService.queryList(new HashMap<String, Object>());
			Long parentId = null;
			for(SysDeptEntity sysDeptEntity : deptList){
				if(parentId == null){
					parentId = sysDeptEntity.getParentId();
					continue;
				}

				if(parentId > sysDeptEntity.getParentId().longValue()){
					parentId = sysDeptEntity.getParentId();
				}
			}
			deptId = parentId;
		}

		return new ResultWrapper<Long>(RespEnum.SUCCESS,deptId);

	}
	
	/**
	 * 信息
	 */
	@RequestMapping(value="/info/{deptId}",method=RequestMethod.GET)
	@RequiresPermissions("sys:dept:info")
	@ApiOperation("信息")
	public ResultWrapper<SysDeptEntity> info(@PathVariable("deptId") Long deptId){
		SysDeptEntity dept = sysDeptService.selectById(deptId);
		return new ResultWrapper<SysDeptEntity>(RespEnum.SUCCESS,dept);
	}
	
	/**
	 * 保存
	 */
	@RequestMapping(value="/save",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:dept:save")
	@ApiOperation("保存")
	public ResultWrapper<Boolean> save(@RequestBody SysDeptEntity dept){
		sysDeptService.insert(dept);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 修改
	 */
	@RequestMapping(value="/update",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:dept:update")
	@ApiOperation("修改")
	public ResultWrapper<Boolean> update(@RequestBody SysDeptEntity dept){
		sysDeptService.updateById(dept);
		
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 删除
	 */
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	@RequiresPermissions("sys:dept:delete")
	@ApiOperation("删除")
	public ResultWrapper<Boolean> delete(long deptId){
		//判断是否有子部门
		List<Long> deptList = sysDeptService.queryDetpIdList(deptId);
		if(deptList.size() > 0){
			return new ResultWrapper<Boolean>(RespEnum.PLEASE_DELETE_THE_SUB_DEPARTMENT_FIRST);
		}
		sysDeptService.deleteById(deptId);
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
}
