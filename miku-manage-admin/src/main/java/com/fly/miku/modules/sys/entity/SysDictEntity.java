package com.fly.miku.modules.sys.entity;

import java.io.Serializable;
import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableLogic;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 数据字典
 *
 * @author Flying flying_miku@sina.com
 * @since 3.1.0 2018-01-27
 */
@TableName("sys_dict")
@Data
@ApiModel("数据字典")
public class SysDictEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId
	@ApiModelProperty(value="ID")
	private Long id;
	/**
	 * 字典名称
	 */
	@NotBlank(message="字典名称不能为空")
	@ApiModelProperty(value="字典名称")
	private String name;
	/**
	 * 字典类型
	 */
	@NotBlank(message="字典类型不能为空")
	@ApiModelProperty(value="字典类型")
	private String type;
	/**
	 * 字典码
	 */
	@NotBlank(message="字典码不能为空")
	@ApiModelProperty(value="字典码")
	private String code;
	/**
	 * 字典值
	 */
	@NotBlank(message="字典值不能为空")
	@ApiModelProperty(value="字典值")
	private String value;
	/**
	 * 排序
	 */
	@ApiModelProperty(value="排序")
	private Integer orderNum;
	/**
	 * 备注
	 */
	@ApiModelProperty(value="备注")
	private String remark;
	/**
	 * 删除标记  -1：已删除  0：正常
	 */
	@TableLogic
	@ApiModelProperty(value="删除标记  -1：已删除  0：正常")
	private Integer delFlag;

}
