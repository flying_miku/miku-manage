package com.fly.miku.modules.oss.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fly.miku.modules.oss.entity.SysOssEntity;

/**
 * 文件上传
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-03-25 12:13:26
 */
public interface SysOssDao extends BaseMapper<SysOssEntity> {
	
}
