package com.fly.miku.modules.sys.controller;


import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.modules.sys.shiro.ShiroUtils;
import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 登录相关
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2016年11月10日 下午1:15:31
 */
@Controller
@Api(tags = "登录相关API")
public class SysLoginController {
	@Autowired
	private Producer producer;
	
	@RequestMapping(value="captcha.jpg",method=RequestMethod.GET)
	@ApiOperation("验证码")
	public void captcha(HttpServletResponse response)throws IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");

        //生成文字验证码
        String text = producer.createText();
        //生成图片验证码
        BufferedImage image = producer.createImage(text);
        //保存到shiro session
        ShiroUtils.setSessionAttribute(Constants.KAPTCHA_SESSION_KEY, text);
        
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
	}
	
	/**
	 * 登录
	 */
	@ResponseBody
	@RequestMapping(value="/sys/login", method = RequestMethod.POST)
	@ApiOperation("登录")
	public ResultWrapper<Boolean> login(String username, String password, String captcha) {
		/*String kaptcha = ShiroUtils.getKaptcha(Constants.KAPTCHA_SESSION_KEY);
		if(!captcha.equalsIgnoreCase(kaptcha)){
			return new ResultWrapper<Boolean>(RespEnum.VALIDATION_CODE_IS_INCORRECT);
		}*/
		
		try{
			Subject subject = ShiroUtils.getSubject();
			UsernamePasswordToken token = new UsernamePasswordToken(username, password);
			subject.login(token);
		}catch (UnknownAccountException e) {
			return new ResultWrapper<Boolean>(RespEnum.INCORRECT_ACCOUNT_OR_PASSWORD,e.getMessage());
		}catch (IncorrectCredentialsException e) {
			return new ResultWrapper<Boolean>(RespEnum.INCORRECT_ACCOUNT_OR_PASSWORD);
		}catch (LockedAccountException e) {
			return new ResultWrapper<Boolean>(RespEnum.THE_ACCOUNT_HAS_BEEN_LOCKED_PLEASE_CONTACT_THE_ADMINISTRATOR);
		}catch (AuthenticationException e) {
			return new ResultWrapper<Boolean>(RespEnum.ACCOUNT_VERIFICATION_FAILURE);
		}
	    
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	
	/**
	 * 退出
	 *//*
	@RequestMapping(value="logout", method = RequestMethod.GET)
	@ApiOperation("退出")
	public String logout() {
		ShiroUtils.logout();
		return "redirect:login.html";
	}
	*/
}
