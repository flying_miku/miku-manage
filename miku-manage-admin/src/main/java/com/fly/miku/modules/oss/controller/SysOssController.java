package com.fly.miku.modules.oss.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fly.miku.common.exception.FLYException;
import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.ConfigConstant;
import com.fly.miku.common.utils.Constant;
import com.fly.miku.common.utils.PageUtils;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.common.validator.ValidatorUtils;
import com.fly.miku.common.validator.group.AliyunGroup;
import com.fly.miku.common.validator.group.QcloudGroup;
import com.fly.miku.common.validator.group.QiniuGroup;
import com.fly.miku.modules.oss.cloud.CloudStorageConfig;
import com.fly.miku.modules.oss.cloud.OSSFactory;
import com.fly.miku.modules.oss.entity.SysOssEntity;
import com.fly.miku.modules.oss.service.SysOssService;
import com.fly.miku.modules.sys.service.SysConfigService;
import com.google.gson.Gson;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 文件上传
 * 
 * @author Flying
 * @email flying_miku@sina.com
 * @date 2017-03-25 12:13:26
 */
@RestController
@RequestMapping(value="sys/oss")
@Api(tags  = "文件上传API")
public class SysOssController {
	@Autowired
	private SysOssService sysOssService;
    @Autowired
    private SysConfigService sysConfigService;

    private final static String KEY = ConfigConstant.CLOUD_STORAGE_CONFIG_KEY;
	
	/**
	 * 列表
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@RequiresPermissions("sys:oss:all")
	@ApiOperation("列表")
	public ResultWrapper<PageUtils> list(@RequestParam Map<String, Object> params){
		PageUtils page = sysOssService.queryPage(params);
		return new ResultWrapper<PageUtils>(RespEnum.SUCCESS,page);
	}


    /**
     * 云存储配置信息
     */
    @RequestMapping(value="/config",method=RequestMethod.GET)
    @RequiresPermissions("sys:oss:all")
	@ApiOperation("云存储配置信息")
    public ResultWrapper<CloudStorageConfig> config(){
        CloudStorageConfig config = sysConfigService.getConfigObject(KEY, CloudStorageConfig.class);
		return new ResultWrapper<CloudStorageConfig>(RespEnum.SUCCESS,config);
    }


	/**
	 * 保存云存储配置信息
	 */
	@RequestMapping(value="/saveConfig",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:oss:all")
	@ApiOperation("保存云存储配置信息")
	public ResultWrapper<Boolean> saveConfig(@RequestBody CloudStorageConfig config){
		//校验类型
		ValidatorUtils.validateEntity(config);

		if(config.getType() == Constant.CloudService.QINIU.getValue()){
			//校验七牛数据
			ValidatorUtils.validateEntity(config, QiniuGroup.class);
		}else if(config.getType() == Constant.CloudService.ALIYUN.getValue()){
			//校验阿里云数据
			ValidatorUtils.validateEntity(config, AliyunGroup.class);
		}else if(config.getType() == Constant.CloudService.QCLOUD.getValue()){
			//校验腾讯云数据
			ValidatorUtils.validateEntity(config, QcloudGroup.class);
		}

        sysConfigService.updateValueByKey(KEY, new Gson().toJson(config));

        return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}
	

	/**
	 * 上传文件
	 */
	@RequestMapping(value="/upload",method=RequestMethod.POST)
	@RequiresPermissions("sys:oss:all")
	@ApiOperation("上传文件")
	public ResultWrapper<String> upload(@RequestParam("file") MultipartFile file) throws Exception {
		if (file.isEmpty()) {
			throw new FLYException(RespEnum.UPLOADING_FILES_CAN_NOT_BE_EMPTY);
		}

		//上传文件
		String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
		String url = OSSFactory.build().uploadSuffix(file.getBytes(), suffix);

		//保存文件信息
		SysOssEntity ossEntity = new SysOssEntity();
		ossEntity.setUrl(url);
		ossEntity.setCreateDate(new Date());
		sysOssService.insert(ossEntity);
		 return new ResultWrapper<String>(url,RespEnum.SUCCESS);
	}


	/**
	 * 删除
	 */
	@RequestMapping(value="/delete",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
	@RequiresPermissions("sys:oss:all")
	@ApiOperation(" 删除")
	public ResultWrapper<Boolean> delete(@RequestBody Long[] ids){
		sysOssService.deleteBatchIds(Arrays.asList(ids));

		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
	}

}
