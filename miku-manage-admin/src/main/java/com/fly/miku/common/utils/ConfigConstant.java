package com.fly.miku.common.utils;

/**
 * 系统参数相关Key
 *
 * @author Flying flying_miku@sina.com
 * @since 1.2.0 2017-03-26
 */
public class ConfigConstant {
    /**
     * 云存储配置KEY
     */
    public final static String CLOUD_STORAGE_CONFIG_KEY = "CLOUD_STORAGE_CONFIG_KEY";
}
