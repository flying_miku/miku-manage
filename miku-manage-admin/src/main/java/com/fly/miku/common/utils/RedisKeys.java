package com.fly.miku.common.utils;

/**
 * Redis所有Keys
 *
 * @author Flying flying_miku@sina.com
 * @since 1.0.0 2017-07-18
 */
public class RedisKeys {

    public static String getSysConfigKey(String key){
        return "sys:config:" + key;
    }

    public static String getShiroSessionKey(String key){
        return "sessionid:" + key;
    }
}
