package com.fly.miku.common.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 *z77z
 */

@Configuration
@EnableCaching
public class RedisConfig extends CachingConfigurerSupport {

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private int port;

    @Value("${spring.redis.timeout}")
    private int timeout;

    @Value("${spring.redis.pool.max-idle}")
    private int maxIdle;

    @Value("${spring.redis.pool.max-wait}")
    private long maxWaitMillis;
    
    @Value("${spring.redis.pool.test-on-borrow}")
    private boolean testOnBorrow;
    
    @Value("${spring.redis.pool.max-total}")
    private int maxTotal;

    @Bean
    public JedisPool redisPoolFactory() {
    	Logger.getLogger(getClass()).info("JedisPool注入成功！！");
        Logger.getLogger(getClass()).info("redis地址：" + host + ":" + port);
        JedisPool jedisPool = new JedisPool(poolConfig(), host, port, timeout);
        return jedisPool;
    }
    
    
    @Bean
    public JedisPoolConfig poolConfig(){
    	JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(maxIdle);
        jedisPoolConfig.setMaxTotal(maxTotal);
        jedisPoolConfig.setMaxWaitMillis(maxWaitMillis);
        jedisPoolConfig.setTestOnBorrow(testOnBorrow);
        return jedisPoolConfig;
    }
    
    @Bean 
    public JedisConnectionFactory connectionFactory(){
    	JedisConnectionFactory connectionFactory = new JedisConnectionFactory();
    	connectionFactory.setHostName(host);
    	connectionFactory.setPort(port);
    	connectionFactory.setPoolConfig(poolConfig());
    	return connectionFactory;
    }
    
  /*  @Bean
    public StringRedisSerializer stringRedisSerializer(){
    	StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
    	return stringRedisSerializer;
    }*/
    
    
    /*@Bean
    public RedisTemplate<String,String> redisTemplate(){
    	RedisTemplate<String,String> redisTemplate = new RedisTemplate<String,String>();
    	redisTemplate.setConnectionFactory(connectionFactory());
    	redisTemplate.setKeySerializer(stringRedisSerializer());
    	redisTemplate.setValueSerializer(stringRedisSerializer());
    	redisTemplate.setEnableTransactionSupport(true);
    	return redisTemplate;
    }*/
    
    @Bean 
    public StringRedisTemplate stringRedisTemplate(){
    	StringRedisTemplate stringRedisTemplate = new StringRedisTemplate(connectionFactory());
    	stringRedisTemplate.setEnableTransactionSupport(true);
    	return stringRedisTemplate;
    }
    
   /* @Bean
    public RedisCacheManager cacheManager(){
    	RedisCacheManager  cacheManager = new RedisCacheManager(redisTemplate());
    	return cacheManager;
    }*/
    
    @Bean
    public RedisCacheManager cacheStringManager(){
    	RedisCacheManager  cacheManager = new RedisCacheManager(stringRedisTemplate());
    	return cacheManager;
    }
    

}
