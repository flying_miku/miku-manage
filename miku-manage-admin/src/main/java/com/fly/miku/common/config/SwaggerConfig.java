package com.fly.miku.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger配置
 *
 * @author Flying flying_miku@sina.com
 * @since 1.0.0 2018-01-16
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig{

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(apiInfo())
            .select()
            //加了ApiOperation注解的类，生成接口文档
            .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
            //包下的类，生成接口文档
            //.apis(RequestHandlerSelectors.basePackage("com.fly.miku.modules.job.controller"))
            .paths(PathSelectors.any())
            .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("API文档")//大标题
            .description("miku-manage接口文档")//详细描述
            .version("1.0.0")//版本
            .contact(new Contact("Flying", "https://my.oschina.net/flyingmiku/blog", "363131422@qq.com"))//作者
            .termsOfServiceUrl("http://about.yidaba.com/service.htm")//条款服务网址
            .license("The Apache License, Version 2.0")//许可证
            .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")//许可证信息
            .build();
    }

}
