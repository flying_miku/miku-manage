package com.fly.miku.common.error.controller;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.ResultWrapper;
import com.fly.miku.modules.gen.utils.PageUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
@RestController
@Slf4j
public class FlyErrorController implements ErrorController {

	private final static String ERROR_PATH = "/error";
	
	private final static String NOT_FOUND = "/404";
	
	/**
	 * Error Attributes in the Application
	 */
	@Autowired
	private ErrorAttributes errorAttributes;





	/**
	 * Supports other formats like JSON, XML
	 *
	 * @param request
	 */
	@RequestMapping(value = ERROR_PATH)
	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResultWrapper<String> error(HttpServletRequest request,HttpServletResponse response) {
		RequestAttributes requestAttributes = new ServletRequestAttributes(request);
		Map<String, Object> body = this.errorAttributes.getErrorAttributes(requestAttributes, getTraceParameter(request));
		log.error("map is [{}]", body);
		String url = request.getRequestURL().toString();
		body.put("URL", url);
		log.error("[error info]: status-{}, request url-{}", body.get("status"), url);
		response.setStatus(HttpStatus.OK.value());
		return new ResultWrapper<String>(RespEnum.SERVER_ERROR);
	}
	
	@RequestMapping(value = NOT_FOUND)
	@ResponseBody
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResultWrapper<String> notFound(HttpServletRequest request,HttpServletResponse response) {
		RequestAttributes requestAttributes = new ServletRequestAttributes(request);
		Map<String, Object> body = this.errorAttributes.getErrorAttributes(requestAttributes, getTraceParameter(request));
		log.info("map is [{}]", body);
		String url = request.getRequestURL().toString();
		body.put("URL", url);
		log.info("[error info]: status-{}, request url-{}", body.get("status"), url);
		response.setStatus(HttpStatus.OK.value());
		return new ResultWrapper<String>(RespEnum.NOT_FOUND_ERROR);
	}
	
	
	
	

	@Override
	public String getErrorPath() {
		return ERROR_PATH;
	}

	@SuppressWarnings("static-method")
	private boolean getTraceParameter(HttpServletRequest request) {
		String parameter = request.getParameter("trace");
		if (parameter == null) {
			return false;
		}
		return !"false".equals(parameter.toLowerCase());
	}



}
