package com.fly.miku.common.exception;

import org.apache.shiro.authz.AuthorizationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.ResultWrapper;

import freemarker.core.InvalidReferenceException;
import lombok.extern.slf4j.Slf4j;

/**
 * 异常处理器
 *
 * @author Flying flying_miku@sina.com
 * @since 1.0.0 2016-10-27
 */
@RestControllerAdvice
@Slf4j
public class FLYExceptionHandler {

	/**
	 * 处理自定义异常
	 */
	@ExceptionHandler(FLYException.class)
	public ResultWrapper<String> handleFLYException(FLYException e){
		return new ResultWrapper<String>(e.getCode(),e.getMessage());
	}

	@ExceptionHandler(DuplicateKeyException.class)
	public ResultWrapper<String> handleDuplicateKeyException(DuplicateKeyException e){
		log.error(e.getMessage(), e);
		return new ResultWrapper<String>(RespEnum.RECORD_EXISTED_DATABASE);
	}

	@ExceptionHandler(AuthorizationException.class)
	public ResultWrapper<String> handleAuthorizationException(AuthorizationException e){
		log.error(e.getMessage(), e);
		return new ResultWrapper<String>(RespEnum.NO_AUTHORITY_PLEASE_CONTACT_THE_ADMINISTRATOR_TO_AUTHORIZE);

	}

	
	@ExceptionHandler(InvalidReferenceException.class)
	public ResultWrapper<String> handleInvalidReferenceException(InvalidReferenceException e){
		log.error(e.getMessage(), e);
		return new ResultWrapper<String>(RespEnum.DATA_ILLEGALITY);
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	public ResultWrapper<String> handleIllegalArgumentException(IllegalArgumentException e){
		log.error(e.getMessage(), e);
		return new ResultWrapper<String>(RespEnum.DATA_ILLEGALITY);
	}
	
	@ExceptionHandler(Exception.class)
	public ResultWrapper<String> handleException(Exception e){
		log.error(e.getMessage(), e);
		return new ResultWrapper<String>(RespEnum.SERVER_ERROR);
	}
	
	@ExceptionHandler(MissingServletRequestParameterException.class)
	public ResultWrapper<String> handleBindingException(MissingServletRequestParameterException e){
		log.error(e.getMessage(), e);
		return new ResultWrapper<String>(RespEnum.REQUEST_PARAM_ERROR.getCode(),e.getParameterName()+"不能为空");
	}
	
	
}
