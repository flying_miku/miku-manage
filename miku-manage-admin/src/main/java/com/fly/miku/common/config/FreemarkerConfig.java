package com.fly.miku.common.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.fly.miku.modules.sys.i18n.I18nTag;
import com.fly.miku.modules.sys.shiro.ShiroTag;

/**
 * Freemarker配置
 *
 * @author Flying flying_miku@sina.com
 * @since 1.0.0 2017-09-28
 */
@Configuration
public class FreemarkerConfig {

	
    @Bean
    public FreeMarkerConfigurer freeMarkerConfigurer(ShiroTag shiroTag,I18nTag i18nTag,MessageSource messageSource){
        FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
        configurer.setTemplateLoaderPath("classpath:/templates");
        Map<String, Object> variables = new HashMap<>(1);
        variables.put("shiro", shiroTag);
        variables.put("i18n", i18nTag);
        configurer.setFreemarkerVariables(variables);
        

        Properties settings = new Properties();
        settings.setProperty("default_encoding", "utf-8");
        settings.setProperty("number_format", "0.##");
        settings.setProperty("classic_compatible","true");
        settings.setProperty("template_exception_handler","rethrow");
        settings.setProperty("locale","zh_CN");  
        settings.setProperty("auto_import", "common/spring.ftl as spring"); 
        configurer.setFreemarkerSettings(settings);
        return configurer;
    }

}
