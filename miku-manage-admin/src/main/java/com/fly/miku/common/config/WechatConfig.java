package com.fly.miku.common.config;

import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.api.impl.WxCpServiceImpl;
import me.chanjar.weixin.cp.message.WxCpMessageRouter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 */
@Configuration
public class WechatConfig {

    @Bean
    public WxCpService getWxCpService(){
        return new WxCpServiceImpl();
    }

    @Bean
    public WxCpMessageRouter getWxCpMessageRouter(){
        return new WxCpMessageRouter(getWxCpService());
    }
}
