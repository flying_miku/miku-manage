package ${package}.${moduleName}.controller;

import java.util.Arrays;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import java.util.Date;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import ${package}.${moduleName}.entity.${className}Entity;
import ${package}.${moduleName}.service.${className}Service;
import ${mainPath}.common.utils.PageUtils;
import com.fly.miku.common.utils.DateUtils;
import com.fly.miku.common.resenum.RespEnum;
import com.fly.miku.common.utils.ResultWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * ${comments}
 *
 * @author ${author}
 * @email ${email}
 * @date ${datetime}
 */
@RestController
@RequestMapping("${moduleName}/${pathName}")
@Api(tags="${comments}api")
public class ${className}Controller {
    @Autowired
    private ${className}Service ${classname}Service;

    /**
     * 列表
     */
    @RequestMapping(value="/list",method=RequestMethod.GET)
    @RequiresPermissions("${moduleName}:${pathName}:list")
    @ApiOperation("列表")
    public ResultWrapper<PageUtils> list(@RequestParam Map<String, Object> params){
        PageUtils page = ${classname}Service.queryPage(params);
		return new ResultWrapper<PageUtils>(RespEnum.SUCCESS,page);
    }


    /**
     * 信息
     */
    @RequestMapping(value="/info/{${pk.firstLowercaseAttrName}}",method=RequestMethod.GET)
    @RequiresPermissions("${moduleName}:${pathName}:info")
    @ApiOperation("信息")
    public ResultWrapper<${className}Entity> info(@PathVariable("${pk.firstLowercaseAttrName}") ${pk.attrType} ${pk.firstLowercaseAttrName}){
			${className}Entity ${classname} = ${classname}Service.selectById(${pk.firstLowercaseAttrName});
			
		return new ResultWrapper<${className}Entity>(RespEnum.SUCCESS,${classname});
    }

    /**
     * 保存
     */
    @RequestMapping(value="/save",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
    @RequiresPermissions("${moduleName}:${pathName}:save")
    @ApiOperation("保存")
    public ResultWrapper<Boolean> save(@RequestBody ${className}Entity ${classname}){
			${classname}Service.insert(${classname});
		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
    }

    /**
     * 修改
     */
    @RequestMapping(value="/update",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
    @RequiresPermissions("${moduleName}:${pathName}:update")
    @ApiOperation("修改")
    public ResultWrapper<Boolean> update(@RequestBody ${className}Entity ${classname}){
			${classname}Service.updateById(${classname});

    		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
    }

    /**
     * 删除
     */
    @RequestMapping(value="/delete",method=RequestMethod.POST,
	        consumes = {"application/json", "application/xml"})
    @RequiresPermissions("${moduleName}:${pathName}:delete")
    @ApiOperation("删除")
    public ResultWrapper<Boolean> delete(@RequestBody ${pk.attrType}[] ${pk.firstLowercaseAttrName}s){
			${classname}Service.deleteBatchIds(Arrays.asList(${pk.firstLowercaseAttrName}s));

    		return new ResultWrapper<Boolean>(RespEnum.SUCCESS);
    }
    
    /**
	 * 导出Excel
	 */
    @RequestMapping(value="/exportExcel",method=RequestMethod.GET)
	@RequiresPermissions("${moduleName}:${pathName}:exportExcel")
	@ApiOperation("导出Excel")
	public void exportExcel(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, Object> params) throws IOException{
		byte[] data =${classname}Service.exportExcel(params);
		response.reset();  
        response.setHeader("Content-Disposition", "attachment; filename=\"${classname}-"+DateUtils.formatDateTime(new Date())+".xls\"");  
        response.addHeader("Content-Length", "" + data.length);  
        response.setContentType("application/octet-stream; charset=UTF-8");  
        IOUtils.write(data, response.getOutputStream());  
	}

}
