layui.config({
  base:'statics/js/'
}).use(['navtab'],function(){
	window.jQuery = window.$ = layui.jquery;
	window.layer = layui.layer;
    var element = layui.element,
	navtab = layui.navtab({
		elem: '.larry-tab-box'
	});

    //iframe自适应
	$(window).on('resize', function() {
		var $content = $('#larry-tab .layui-tab-content');
		$content.height($(this).height() - 140);
	    $content.find('iframe').each(function() {
	    	$(this).height($content.height());
	    });
	}).resize();
  
	$(function(){
	    $('#larry-nav-side').click(function(){
	        if($(this).attr('lay-filter')!== undefined){
	            $(this).children('ul').find('li').each(function(){
	                var $this = $(this);
	                if($this.find('dl').length > 0){
	                   var $dd = $this.find('dd').each(function(){
	                       $(this).on('click', function() {
	                           var $a = $(this).children('a');
	                           var href = $a.data('url');
	                           var icon = $a.children('i:first').data('icon');
	                           var title = $a.children('span').text();
	                           var data = {
	                                 href: href,
	                                 icon: icon,
	                                 title: title
	                           }
	                           navtab.tabAdd(data);
	                       });
	                   });
	                }else{
	                	$this.on('click', function() {
                           var $a = $(this).children('a');
                           var href = $a.data('url');
                           var icon = $a.children('i:first').data('icon');
                           var title = $a.children('span').text();
                           var data = {
                                 href: href,
                                 icon: icon,
                                 title: title
                           }
                           navtab.tabAdd(data);
	                    });
	                }
	            });
	        }
	    }).trigger("click");
	});
});


layui.use(['jquery','layer','element'],function(){
	window.jQuery = window.$ = layui.jquery;
	window.layer = layui.layer;
	var element = layui.element;

	// larry-side-menu向左折叠
	$('.larry-side-menu').click(function() {
	  var sideWidth = $('#larry-side').width();
	  if(sideWidth === 200) {
	      $('#larry-body').animate({
	        left: '0'
	      }); 
	      $('#larry-footer').animate({
	        left: '0'
	      });
	      $('#larry-side').animate({
	        width: '0'
	      });
	  } else {
	      $('#larry-body').animate({
	        left: '200px'
	      });
	      $('#larry-footer').animate({
	        left: '200px'
	      });
	      $('#larry-side').animate({
	        width: '200px'
	      });
	  }
	});
});



//生成菜单
var menuItem = Vue.extend({
	name: 'menu-item',
	props:{item:{}},
	template:[
	          '<li class="layui-nav-item">',
	          '<a v-if="item.type === 0" href="javascript:;">',
	          '<i v-if="item.icon != null" :class="item.icon"></i>',
	          '<span>{{item.name}}</span>',
	          '<em class="layui-nav-more"></em>',
	          '</a>',
	          '<dl v-if="item.type === 0" class="layui-nav-child">',
	          '<dd v-for="item in item.list">',
	          '<a v-if="item.type === 1" href="javascript:;" :data-url="item.url"><i v-if="item.icon != null" :class="item.icon" :data-icon="item.icon"></i> <span>{{item.name}}</span></a>',
	          '</dd>',
	          '</dl>',
	          '<a v-if="item.type === 1" href="javascript:;" :data-url="item.url"><i v-if="item.icon != null" :class="item.icon" :data-icon="item.icon"></i> <span>{{item.name}}</span></a>',
	          '</li>'
	].join('')
});

//注册菜单组件
Vue.component('menuItem',menuItem);

var vm = new Vue({
	el:'#layui_layout',
	data:{
		user:{},
		menuList:{},
		password:'',
		newPassword:'',
        navTitle:$.i18n.map['system.common.console']
	},
	methods: {
		getMenuList: function () {
			$.getJSON("sys/menu/nav?_"+$.now(), function(r){
				vm.menuList =recursionMenuList(r.entity);
			});
		},
		getUser: function(){
			$.getJSON("sys/user/info?_"+$.now(), function(r){
				vm.user = r.entity;
			});
		},
		updatePassword: function(){
			layer.open({
				type: 1,
				skin: 'layui-layer-molv',
				title: $.i18n.map['system.common.modify.the.password'],
				area: ['550px', '270px'],
				shadeClose: false,
				content: jQuery("#passwordLayer"),
				btn: [$.i18n.map['system.common.update'],$.i18n.map['system.common.cancel']],
				btn1: function (index) {
					var data = "password="+vm.password+"&newPassword="+vm.newPassword;
					$.ajax({
						type: "POST",
					    url: "sys/user/password",
					    data: data,
					    dataType: "json",
					    success: function(result){
							if(result.code == 0){
								layer.close(index);
								layer.alert($.i18n.map['system.common.update.success'], function(index){
									location.reload();
								});
							}else{
								layer.alert(result.message);
							}
						}
					});
	            }
			});
		},
        donate: function () {
            layer.open({
                type: 2,
                title: false,
                area: ['337px', '448px'],
                closeBtn: 1,
                shadeClose: false,
                content: ['http://oqe5krj00.bkt.clouddn.com/f505e1252e614128a7ceda5949214dad.jpg', 'no']
            });
        }
	},
	created: function(){
		this.getMenuList();
		this.getUser();
	}
});

function recursionMenuList(menuList){
	for(var menu in menuList){
		if(menuList[menu]['type']==1){
			menuList[menu]['url']=menuList[menu]['url']+"?menuId="+menuList[menu]['menuId']
		}else if(menuList[menu]['type']==0){
			if(menuList[menu].list&&menuList[menu].list.length>0){
				menuList[menu].list = recursionMenuList(menuList[menu].list)
			}
		}
	}
	return menuList;
}



var i18nLanguage = "zh_CN"; 
function getLanguage(){
	if(!jQuery.cookie("language")){
		jQuery.cookie("language",i18nLanguage, {  
	          expires : 7  
	        });
		return i18nLanguage;
	}else{
		return jQuery.cookie("language");
	}
}
jQuery.i18n.properties({
    name: 'messages',
    path: 'statics/js/i18n/', //资源文件路径
    mode: 'map', //用Map的方式使用资源文件中的值
    language: getLanguage(),
    callback: function () {//加载成功后设置显示内容
        console.log("i18n赋值中...");
        try {
        	 console.log($.i18n.prop("system.common.name"));
        }
        catch(ex){ }
        console.log("i18n写入完毕");
    }
});

