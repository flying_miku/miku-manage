//生成菜单
var menuItem = Vue.extend({
    name: 'menu-item',
    props:{item:{}},
    template:[
        '<li>',
        '	<a v-if="item.type === 0" href="javascript:;">',
        '		<i v-if="item.icon != null" :class="item.icon"></i>',
        '		<span>{{item.name}}</span>',
        '		<i class="fa fa-angle-left pull-right"></i>',
        '	</a>',
        '	<ul v-if="item.type === 0" class="treeview-menu">',
        '		<menu-item :item="item" v-for="item in item.list"></menu-item>',
        '	</ul>',

        '	<a v-if="item.type === 1 && item.parentId === 0" :href="\'#\'+item.url">',
        '		<i v-if="item.icon != null" :class="item.icon"></i>',
        '		<span>{{item.name}}</span>',
        '	</a>',

        '	<a v-if="item.type === 1 && item.parentId != 0" :href="\'#\'+item.url"><i v-if="item.icon != null" :class="item.icon"></i><i v-else class="fa fa-circle-o"></i> {{item.name}}</a>',
        '</li>'
    ].join('')
});

//iframe自适应
$(window).on('resize', function() {
	var $content = $('.content');
	$content.height($(this).height() - 120);
	$content.find('iframe').each(function() {
		$(this).height($content.height());
	});
}).resize();

//注册菜单组件
Vue.component('menuItem',menuItem);

var vm = new Vue({
	el:'#rrapp',
	data:{
		user:{},
		menuList:{},
		main:"main.html",
		password:'',
		newPassword:'',
        navTitle:"控制台"
	},
	methods: {
		getMenuList: function (event) {
			$.getJSON("sys/menu/nav?_"+$.now(), function(r){
				vm.menuList =recursionMenuList(r.entity);
			});
		},
		getUser: function(){
			$.getJSON("sys/user/info?_"+$.now(), function(r){
				vm.user = r.entity;
			});
		},
		updatePassword: function(){
			layer.open({
				type: 1,
				skin: 'layui-layer-molv',
				title: $.i18n.map['system.common.modify.the.password'],
				area: ['550px', '270px'],
				shadeClose: false,
				content: jQuery("#passwordLayer"),
				btn: [$.i18n.map['system.common.update'],$.i18n.map['system.common.cancel']],
				btn1: function (index) {
					var data = "password="+vm.password+"&newPassword="+vm.newPassword;
					$.ajax({
						type: "POST",
					    url: "sys/user/password",
					    data: data,
					    dataType: "json",
					    success: function(result){
							if(result.code == 0){
								layer.close(index);
								layer.alert($.i18n.map['system.common.update.success'], function(index){
									location.reload();
								});
							}else{
								layer.alert(result.message);
							}
						}
					});
	            }
			});
		},
        donate: function () {
            layer.open({
                type: 2,
                title: false,
                area: ['337px', '448px'],
                closeBtn: 1,
                shadeClose: false,
                content: ['http://oqe5krj00.bkt.clouddn.com/f505e1252e614128a7ceda5949214dad.jpg', 'no']
            });
        }
	},
	created: function(){
		this.getMenuList();
		this.getUser();
	},
	updated: function(){
		//路由
		var router = new Router();
		routerList(router, vm.menuList);
		router.start();
	}
});


function recursionMenuList(menuList){
	for(var menu in menuList){
		if(menuList[menu]['type']==1){
			menuList[menu]['url']=menuList[menu]['url']+"?menuId="+menuList[menu]['menuId']
		}else if(menuList[menu]['type']==0){
			if(menuList[menu].list&&menuList[menu].list.length>0){
				menuList[menu].list = recursionMenuList(menuList[menu].list)
			}
		}
	}
	return menuList;
}
function routerList(router, menuList){
	for(var key in menuList){
		var menu = menuList[key];
		if(menu.type == 0){
			routerList(router, menu.list);
		}else if(menu.type == 1){
			router.add('#'+menu.url, function() {
				var url = window.location.hash;
				
				//替换iframe的url
			    vm.main = url.replace('#', '');
			    
			    //导航菜单展开
			    $(".treeview-menu li").removeClass("active");
			    $("a[href='"+url+"']").parents("li").addClass("active");
			    
			    vm.navTitle = $("a[href='"+url+"']").text();
			});
		}
	}
}


var i18nLanguage = "zh_CN"; 
function getLanguage(){
	if(!jQuery.cookie("language")){
		jQuery.cookie("language",i18nLanguage, {  
	          expires : 7  
	        });
		return i18nLanguage;
	}else{
		return jQuery.cookie("language");
	}
}
jQuery.i18n.properties({
    name: 'messages',
    path: 'statics/js/i18n/', //资源文件路径
    mode: 'map', //用Map的方式使用资源文件中的值
    language: getLanguage(),
    callback: function () {//加载成功后设置显示内容
        console.log("i18n赋值中...");
        try {
        	 console.log($.i18n.prop("system.common.name"));
        }
        catch(ex){ }
        console.log("i18n写入完毕");
    }
});


