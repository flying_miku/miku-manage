$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/log/list',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', width: 30, key: true },
			{ label: '用户名', editable: true,edittype: 'text', name: 'username', width: 50 }, 			
			{ label: '用户操作', editable: true,edittype: 'text',name: 'operation', width: 70 }, 			
			{ label: '请求方法', editable: true,edittype: 'text',name: 'method', width: 150 }, 			
			{ label: '请求参数', editable: true,edittype: 'file',name: 'params', width: 80 },
            { label: '执行时长(毫秒)', editable: true,edittype: 'text',name: 'time', width: 80 },
			{ label: 'IP地址', editable: true,edittype: 'text',name: 'ip', width: 70 }, 			
			{ label: '创建时间', editable: true,edittype: 'text',name: 'createDate', width: 90 }			
        ],
		viewrecords: true,
        height: 450,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: false,
        pager: "#jqGridPager",
        jsonReader : {
            root: "entity.list",
            page: "entity.currPage",
            total: "entity.totalPage",
            records: "entity.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			key: null
		},
	},
	methods: {
		query: function () {
			vm.reload();
		},
		reload: function (event) {
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
				postData:{'key': vm.q.key},
                page:page
            }).trigger("reloadGrid");
		}
	}
});