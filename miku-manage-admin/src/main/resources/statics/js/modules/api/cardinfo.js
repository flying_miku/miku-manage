
var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			content: null
		},
		showList: true,
		title: null,
		cardInfo: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = $.i18n.map['system.common.add'];
			vm.cardInfo = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = $.i18n.map['system.common.update'];
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
			var url = vm.cardInfo.id == null ? "api/cardinfo/save" : "api/cardinfo/update";
			if(vm.cardInfo.positiveImage==null||vm.cardInfo.positiveImage==''){
				alert("请上传名片正面图片");
				return;
			}
			if(vm.cardInfo.negativeImage==null||vm.cardInfo.negativeImage==''){
				alert("请上传名片反面图片");
				return;
			}
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.cardInfo),
			    success: function(r){
			    	if(r.code === 0){
						alert($.i18n.map['system.common.success'],function(index){
							vm.reload();
						});
					}else{
						alert(r.message);
					}
				}
			});
		},
		del: function (event) {
			var ids = getSelectedRow();
			if(ids == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "api/cardinfo/delete",
                    contentType: "application/json",
				    data: JSON.stringify([ids]),
				    success: function(r){
						if(r.code == 0){
							alert($.i18n.map['system.common.success'], function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.message);
						}
					}
				});
			});
		},
		backup: function (event) {
				$.ajax({
					type: "POST",
				    url: baseURL + "api/cardinfo/databaseBackup",
				    data: {path:'D:/程序/backup'},
				    success: function(r){
						if(r.code == 0){
							alert(r.message);
						}else{
							alert(r.message);
						}
					}
				});
		},
		getInfo: function(id){
			$.get(baseURL + "api/cardinfo/info/"+id, function(r){
                vm.cardInfo = r.entity;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
				postData:{'content': vm.q.content},
                page:page
            }).trigger("reloadGrid");
		}
	}
});
$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'api/cardinfo/list',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', index: 'id', width: 50, key: true },
			{ label: '内容', name: 'content', index: 'content', width: 80 }, 			
			{ label: '创建时间', name: 'creaDate', index: 'crea_date', width: 80 }, 			
			{ label: '正面图片', name: 'positiveImage', index: 'positive_image', width: 80 }, 			
			{ label: '反面图片', name: 'negativeImage', index: 'negative_image', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: false,
        pager: "#jqGridPager",
        jsonReader : {
            root: "entity.list",
            page: "entity.currPage",
            total: "entity.totalPage",
            records: "entity.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        }, 
        onSelectRow: function(ids) { //单击选择行  
            if(ids != null) {   
            	var pic_data =  $("#jqGrid").getRowData(ids);
            	 $("#pic1").attr("src",pic_data.positiveImage);
            	 $("#pic2").attr("src",pic_data.negativeImage);
            }  
        }, 
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
    
    new AjaxUpload('#positiveImage', {
        action: baseURL + "sys/oss/upload",
        name: 'file',
        autoSubmit:true,
        responseType:"json",
        onSubmit:function(file, extension){
            if (!(extension && /^(jpg|jpeg|png|gif)$/.test(extension.toLowerCase()))){
                alert('只支持jpg、png、gif格式的图片！');
                return false;
            }
        },
        onComplete : function(file, r){
            if(r.code == 0){
           	 vm.cardInfo.positiveImage = r.entity;
           	 $("#positiveImage").attr("src",r.entity);
            }else{
                alert(r.msg);
            }
        }
    });
    
    new AjaxUpload('#negativeImage', {
        action: baseURL + "sys/oss/upload",
        name: 'file',
        autoSubmit:true,
        responseType:"json",
        onSubmit:function(file, extension){
            if (!(extension && /^(jpg|jpeg|png|gif)$/.test(extension.toLowerCase()))){
                alert('只支持jpg、png、gif格式的图片！');
                return false;
            }
        },
        onComplete : function(file, r){
            if(r.code == 0){
            	 vm.cardInfo.negativeImage = r.entity;
            	 $("#negativeImage").attr("src",r.entity);
            }else{
                alert(r.msg);
            }
        }
    });
   
});



