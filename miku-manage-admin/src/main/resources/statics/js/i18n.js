
/**
 * 设置语言类型： 默认为中文 
 */
var i18nLanguage = "zh"; 
function getLanguage(){
	if(!jQuery.cookie("language")){
		jQuery.cookie("language",i18nLanguage, {  
	          expires : 7  
	        });
		return i18nLanguage;
	}else{
		return jQuery.cookie("language");
	}
}


  
jQuery.i18n.properties({
    name: 'messages',
    path: '../../statics/js/i18n/', //资源文件路径
    mode: 'map', //用Map的方式使用资源文件中的值
    language: getLanguage(),
    callback: function () {//加载成功后设置显示内容
      /*  console.log("i18n赋值中...");
        try {
        	 console.log($.i18n.prop("system.common.name"));
        }
        catch(ex){ }*/
        console.log("i18n写入完毕");
    }
}); 

